<?php

namespace Modules\Atraction\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Modules\Atraction\Repositories\AtractionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Atraction\Events\AtractionWasCreated;
use Modules\Atraction\Events\AtractionWasDeleted;
use Modules\Atraction\Events\AtractionWasUpdated;

class EloquentAtractionRepository extends EloquentBaseRepository implements AtractionRepository {

    /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data) {
        $atraction = $this->model->create($data);
        event(new AtractionWasCreated($atraction->id, $data, $atraction));

        return $atraction;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data) {
        $model->update($data);
        event(new AtractionWasUpdated($model->id, $data, $model));

        return $model;
    }

    public function destroy($atraction) {
        event(new AtractionWasDeleted($atraction));

        return $atraction->delete();
    }
}
