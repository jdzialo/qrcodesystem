<?php

return [
    'list resource' => 'List galleries',
    'create resource' => 'Create galleries',
    'edit resource' => 'Edit galleries',
    'destroy resource' => 'Destroy galleries',
    'title' => [
        'galleries' => 'Gallery',
        'create gallery' => 'Create a gallery',
        'edit gallery' => 'Edit a gallery',
    ],
    'button' => [
        'create gallery' => 'Create a gallery',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
