<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Status
 * @package App
 */
class Status extends Model {
    const ACTIVE = 1;
    const INACTIVE = 2;
    
    /**
     * @var array
     */
    private $statuses = [];

    public function __construct() {
        $this->statuses = [
            self::ACTIVE => trans('core::core.status.active'),
            self::INACTIVE => trans('core::core.status.inactive'),
        ];
    }

    /**
     * Get the available statuses
     * @return array
     */
    public function lists() {
        return $this->statuses;
    }

    /**
     * Get the post status
     * @param int $statusId
     * @return string
     */
    public function get($statusId) {
        if (isset($this->statuses[$statusId])) {
            return $this->statuses[$statusId];
        }

        return $this->statuses[self::INACTIVE];
    }
    
}
