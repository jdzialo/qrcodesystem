<?php

namespace Modules\Gallery\Events;

use Modules\Media\Contracts\StoringMedia;
use Modules\Gallery\Entities\Gallery;
use Modules\Gallery\Entities\Image;

class GalleryWasUpdated implements StoringMedia {

    /**
     * @var array
     */
    public $data;

    /**
     * @var int
     */
    public $galleryId;

    /**
     * @var gallery
     */
    public $gallery;

    public function __construct($galleryId, array $data, $gallery) {
        $this->data = $data;
        foreach ($data['medias_multi']['gallery'] as $order => $file_id) { //[files]
            $image = Image::firstOrCreate([
              'gallery_id' => $galleryId,
              'file_id'   => $file_id,
            ]);
            $image->order = $order;
            $image->save();
        }
        $this->galleryId = $galleryId;
        $this->gallery = $gallery;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData() {
        return $this->data;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity() {
        return $this->gallery;
    }

}
