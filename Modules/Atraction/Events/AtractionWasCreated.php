<?php

namespace Modules\Atraction\Events;

use Modules\Media\Contracts\StoringMedia;
use Modules\Atraction\Entities\Atraction;

class AtractionWasCreated implements StoringMedia {

    /**
     * @var array
     */
    public $data;

    /**
     * @var int
     */
    public $atractionId;

    /**
     * @var Page
     */
    public $atraction;

    public function __construct($atractionId, array $data, $atraction) {
        $this->data = $data;
        $this->atractionId = $atractionId;
        $this->atraction = $atraction;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData() {
        return $this->data;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity() {
        return $this->atraction;
    }

}
