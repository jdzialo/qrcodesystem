<?php

return [
    'title'  => 'Category', 
    'titles' => [
        'category' => 'Manage Category',
        'create category' => 'Create category',
        'edit category' => 'Edit category',
        'create category item' => 'Create undercategory',
        'edit category item' => 'Edit undercategory',
    ],
    'breadcrumb' => [
        'category' => 'Manage categoty',
        'create category' => 'Create category',
        'edit category' => 'Edit category',
        'create category item' => 'Create undercategory',
        'edit category item' => 'Edit undercategory',
    ],
    'button' => [
        'create category item' => 'Create undercategory',
        'create category' => 'Create category',
    ],
    'table' => [
        'name' => 'Name',
        'title' => 'Title',
    ],
    'form' => [
        'title' => 'Title',
        'name' => 'Name',
        'status' => 'Status',
        'uri' => 'URI',
        'url' => 'URL',
        'primary' => 'Primary category (used for front-end routing)',
    ],
    'navigation' => [
        'back to index' => 'Go back to the category index',
    ],
    'list resource' => 'List categorys',
    'create resource' => 'Create categorys',
    'edit resource' => 'Edit categorys',
    'destroy resource' => 'Delete categorys',
];
