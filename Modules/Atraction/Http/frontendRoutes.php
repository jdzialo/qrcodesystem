<?php

use Illuminate\Routing\Router;

$router->group(['prefix' => LaravelLocalization::transRoute('routes.atractions')], function (Router $router) {
//    $locale = LaravelLocalization::setLocale() ?: App::getLocale();
    $router->get(LaravelLocalization::transRoute('routes.guide'), [
        'as' => 'atraction.list',
        'uses' => 'PublicController@index'
    ]);
    $router->post(LaravelLocalization::transRoute('routes.guide').'/filter', [
        'as' => 'atraction.filter',
        'uses' => 'PublicController@filter'
    ]);
    $router->get('/{slug}', [
        'as' => 'atraction.slug',
        'uses' => 'PublicController@show'
    ]);
});