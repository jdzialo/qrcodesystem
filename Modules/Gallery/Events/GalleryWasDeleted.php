<?php

namespace Modules\Gallery\Events;

use Modules\Media\Contracts\DeletingMedia;

class GalleryWasDeleted implements DeletingMedia {

    /**
     * @var string
     */
    private $galleryClass;

    /**
     * @var int
     */
    private $galleryId;

    /**
     * @var object
     */
    public $gallery;

    public function __construct($gallery) {
        $this->$gallery = $gallery;
        $this->gallery = $gallery->id;
        $this->postClass = get_class($gallery);
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId() {
        return $this->galleryId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName() {
        return $this->galleryClass;
    }

}