@extends('layouts.master')

@section('title')
{{ $page->title }} | @parent
@stop

@section('meta')
<meta name="title" content="{{ $page->meta_title}}" />
<meta name="description" content="{{ $page->meta_description }}" />
@stop


@section('content')
<div class="row">
    @if(Session::has('message'))
    <div class="alert alert-info">
        {{Session::get('message')}}
    </div>
    @endif       
</div>
<div class="row">
    <div class="col-sm-6 col-xs-12 slide" style="">
        <!--
        background: url('/assets/images/slide_one.gif');
        
        <img src="/assets/images/slide_one.gif" style="z-index: -1"/>
        -->
    </div>
    <div class="col-sm-6 col-xs-12 bluecontainerslim">
        <div class="editarea">
            {{ $page->title }}
        </div>
    </div>
</div>
<div class="container">
    {!! $page->body !!} 
</div>
@include('offer.offer')
<div class="container">
    <div class="row bottom-buffer" >
        <div class="betweendiv">
            <div class="col-sm-6 banner">

            </div> 
            <div class="col-sm-6 divinformationright" style="display: table;">
                <div style="display: table-cell; vertical-align: middle; height: 100%;">
                    <h1>Zobacz jak dziala RAMPA </h1>
                    <p>
                        Aby lepiej przedstawić Państwu zastosowanie naszej 
                        rampy przygotowaliśmy galerię zdjęć oraz prezentacje wideo.
                    </p>
                    <div>
                        <a href="/gallery"> <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;">
                        <button type="submit" class="btn btn-default btn-blue btn-blue-small">
                            </i>  Przejdź do galerii
                        </button>
                                </a>
                    </div>
                </div>
            </div> 
            <div class="buttonbetween">
                <!--<button type="submit" class="btn btn-default circle-button camera2"></button>-->
                <div class="circle-button camera2">
                </div>
            </div>
        </div>
    </div>
</div>
@stop
