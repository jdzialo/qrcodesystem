<?php

namespace Modules\Atraction\Entities;

use Illuminate\Database\Eloquent\Model;

class AtractionTranslation extends Model {

    public $timestamps = false;
    protected $fillable = [
        'title',
        'subtitle',
        'slug',
        'content_short',
        'content',
        'film',
        'content2',
//        'quotation1',
//        'quotation2',
        
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
    ];
    protected $table = 'atraction__atraction_translations';

}
