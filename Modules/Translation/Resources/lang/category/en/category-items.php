<?php

return [
    'form' => [
        'page' => 'Page',
        'module' => 'Module',
        'target' => 'Target',
        'same tab' => 'Same Tab',
        'new tab' => 'New Tab',
        'icon' => 'Icon',
        'class' => 'Class',
        'parent category item' => 'Parent',
    ],
    'link-type' => [
        'link type' => 'Link type',
        'page' => 'Page',
        'internal' => 'Internal',
        'external' => 'External',
    ],
    'list resource' => 'List category items',
    'create resource' => 'Create category items',
    'edit resource' => 'Edit category items',
    'destroy resource' => 'Delete category items',
];
