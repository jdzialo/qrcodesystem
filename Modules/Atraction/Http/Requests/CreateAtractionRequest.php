<?php

namespace Modules\Atraction\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateAtractionRequest extends BaseFormRequest {

    protected $translationsAttributesKey = 'atraction::atractions.validation.attributes';

    public function rules() {
        return [
            'template' => 'required',
            'visiting_time' => 'required',
            'is_main' => 'required'
        ];
    }

    public function translationRules() {
        return [
            'title' => 'required',
            'slug' => 'required',
        ];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [
            'template.required' => trans('atraction::messages.template is required'),
            'visiting_time.required' => trans('atraction::messages.visiting_time is required'),
            'is_main' => trans('place::places.mainplace is required')
        ];
    }

    public function translationMessages() {
        return [
            'title.required' => trans('atraction::messages.title is required'),
            'slug.required' => trans('atraction::messages.slug is required')
        ];
    }

}
