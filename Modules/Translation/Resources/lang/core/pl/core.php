<?php

return [
    'modal' => [
        'title' => 'Potwierdzenie',
        'confirmation-message' => 'Czy na pewno usunąć ten wpis?',
    ],
    'table' => [
        'created at' => 'Utworzony',
        'actions' => 'Akcje',
        'thumbnail' => 'Miniatura',
        'filename' => 'Nazwa pliku',
    ],
    'tab' => [
        'english' => 'Angielski',
        'french' => 'Francusku',
        'dutch' => 'Holenderski',
        'italian' => 'Włoski',
        'greek' => 'Grecki',
        'spanish' => 'Hiszpański',
        'polish' => 'Polski',
        'polski' => 'Polski',
        'czech' => 'Czech',
        'russian' => 'Rosyjski'
    ],
    'button' => [
        'cancel' => 'Anuluj',
        'create' => 'Utwórz',
        'update' => 'Aktualizuj',
        'delete' => 'Usuń',
        'reset' => 'Resetuj',
        'update and back' => 'Aktualizuj i wróć',
    ],
    'breadcrumb' => [
        'home' => 'Home',
    ],
    'title' => [
        'translatable fields' => 'Pola do tłumaczenia',
        'non translatable fields' => 'Pola nietłumaczalne',
        'create resource' => 'Utwórz :name',
        'edit resource' => 'Edytuj :name',
    ],
    'general' => [
        'available keyboard shortcuts' => 'Dostępne skróty klawiszowe na tej stronie',
        'view website' => 'Zobacz stronę',
        'complete your profile' => 'Uzupełnij swój profil',
        'sign out' => 'Wypisz się',
    ],
    'messages' => [
        'resource created' => ':name poprawnie utworzony/na.',
        'resource not found' => ':name nie znaleziony.',
        'resource updated' => ':name poprawnie zaktualizowany/na.',
        'resource deleted' => ':name poprawnie usunięty/ta.',
    ],
    'back' => 'Powrót',
    'back to index' => 'Powrót do :name',
    'permission denied' => 'Brak dostępu. (wymagane pozwolenie: ":permission")',
    'yes' => 'Tak',
    'no' => 'Nie',
    'status' => [
        'active' => 'Aktywny',
        'inactive' => 'Nieaktywny',
    ],
];
