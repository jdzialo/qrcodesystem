<?php

return [
    'title' => [
        'slides' => 'Slajdy',
    ],
    'popup' => [
        'slide_content' => 'Opis slajdu',
        'title' => 'Tytuł',
        'content' => 'Opis',
        'url' => 'Link',
    ],
];


