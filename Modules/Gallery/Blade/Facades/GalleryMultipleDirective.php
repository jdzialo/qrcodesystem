<?php

namespace Modules\Gallery\Blade\Facades;

use Illuminate\Support\Facades\Facade;

class GalleryMultipleDirective extends Facade {

    protected static function getFacadeAccessor() {
        return 'gallery.multiple.directive';
    }

}
