<?php

namespace Modules\Page\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Page\Entities\Page;
use Modules\Page\Http\Requests\CreatePageRequest;
use Modules\Page\Http\Requests\CreateProductRequest;
use Modules\Page\Http\Requests\UpdateProductRequest;
use Modules\Page\Http\Requests\UpdatePageRequest;
use Modules\Page\Repositories\PageRepository;
use Modules\Media\Repositories\FileRepository;
use Modules\Page\Repositories\ContactRepository;
use Endroid\QrCode\QrCode;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Modules\Category\Repositories\CategoryItemRepository;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Modules\Page\Entities\File;
use Modules\Page\Entities\Product;
use Modules\Category\Entities\CategoryitemTranslation;
use Modules\Category\Entities\Categoryitem;
use Exception;
use DB;
use Modules\Page\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Modules\Category\Repositories\CategoryRepository;
use  Illuminate\Support\Facades\Log;

class PageController extends AdminBaseController {

    /**
     * @var PageRepository
     */
    private $page;

    /**
     * @var FileRepository
     */
    private $file;

    /**
     * @var ContactRepository
     */
    private $contact;

    public function __construct(PageRepository $page, \Modules\Media\Repositories\FileRepository $file, ContactRepository $contact, CategoryItemRepository $categoryItem, ProductRepository $product, CategoryRepository $category, \Modules\Page\Repositories\FileRepository $_file) {
        parent::__construct();
        $this->page = $page;
        $this->file = $file;
        $this->_file = $_file;
        $this->contact = $contact;
        $this->categoryItem = $categoryItem;
        $this->product = $product;
        $this->category = $category;
        $this->assetPipeline->requireCss('icheck.blue.css');
    }

    public function index() {
        $pages = $this->page->all();

        return view('page::admin.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $this->assetPipeline->requireJs('ckeditor.js');

        return view('page::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePageRequest $request
     * @return Response
     */
    public function store(CreatePageRequest $request) {
        $this->page->create($request->all());

        return redirect()->route('admin.page.page.index')
                        ->withSuccess(trans('page::messages.page created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Page $page
     * @return Response
     */
    public function edit(Page $page) {
        $gallery = $this->file->findFileByZoneForEntity('gallery', $page);
        $this->assetPipeline->requireJs('ckeditor.js');

        return view('page::admin.edit', compact('page', 'gallery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Page $page
     * @param  UpdatePageRequest $request
     * @return Response
     */
    public function update(Page $page, UpdatePageRequest $request) {
        $this->page->update($page, $request->all());

        if ($request->get('button') === 'index') {
            return redirect()->route('admin.page.page.index')
                            ->withSuccess(trans('page::messages.page updated'));
        }

        return redirect()->back()
                        ->withSuccess(trans('page::messages.page updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Page $page
     * @return Response
     */
    public function destroy(Page $page) {
        $this->page->destroy($page);

        return redirect()->route('admin.page.page.index')
                        ->withSuccess(trans('page::messages.page deleted'));
    }

    public function show() {

        $contacts = $this->contact->allTranslatedIn(app()->getLocale());
        // $contacts = $this->contact->all();

        return view('page::admin.showcontacts', compact('contacts'));
    }

    public function downloadqrcode(Product $product) {
        $contentLength = $product->files()->get()->first()->filesize;

        $extension = $product->files()->get()->first()->extension;
        if (is_null($extension)) {
            throw new \Exception('Brak zawartości kodu');
        }
        $code_index = $product->files()->get()->first()->code_index;
        if (is_null($code_index)) {
            throw new \Exception('Brak rozszerzenia kodu');
        }
        $url = "/assets/images/" . $code_index . "." . $extension;
        $qrurl = 'http://' . $_SERVER['HTTP_HOST'] . "/assets/images/" . $code_index . "." . $extension;

        header('Content-Description: File Transfer');
        header('Content-Type: application/force-download');
        header('Content-Length: ' . 1699);
        header('Content-Disposition: attachment; filename=' . $code_index . "." . $extension);
        readfile($qrurl);

        exit;
    }

    public function updateproducts(Product $product, UpdateProductRequest $request) {

        $destinationPath = 'assets/files';

        $undercategory = $request->get('undercategory');
        if (is_null($undercategory)) {
            throw new \Exception('Brak wybranej kategorii');
        }

        DB::beginTransaction();

        try {
            $file = $request->file;
            $_file = null;
            $oldFile = $product->files()->get()->first();
            $url = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $destinationPath . '/';

            if (is_null($file)) {
                $_file = $oldFile;
                $qrurl = $url . $_file->filename;
            } else {

                $format = $request->get('format');

                if (is_null($format)) {
                    $format = $oldFile->format;
                }
                $code_index = Str::random(32);
                $fileName = $file->getClientOriginalName();
                $fileName = urlencode($fileName);
                $filesize = $file->getSize();
                $qrurl = $url . $fileName;

                $file->move(
                        $destinationPath, $fileName);
                $pathOldFile = 'assets/files/' . $oldFile->filename;

                if (@file_exists($pathOldFile) && $oldFile->filename != $fileName) {
                    unlink($pathOldFile);
                      //$this->product->destroy($oldFile);
                }

                $_file = new File();
                $_file->filename = $fileName;
                $_file->path = $qrurl;

                $_file->filesize = $filesize;
                $_file->code_index = $code_index;
                $_file->extension = $format;
                $_file->save();

                $oldCodeIndex = $oldFile->code_index;
                $oldExtension = $oldFile->extension;
                $pathToOldQrCode = 'assets/images/' . $oldCodeIndex . "." . $oldExtension;

                if (@file_exists($pathToOldQrCode)) {

                    unlink($pathToOldQrCode);
                }
                $qrCodePath = "assets/images/" . $code_index . "." . $format;

                $qrCode = new QrCode();
                $qrCode
                        ->setText($qrurl)
                        ->setSize(300)
                        ->setPadding(10)
                        ->setErrorCorrection('high')
                        ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                        ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                        ->setLabel('')
                        ->setLabelFontSize(16)
                        ->setImageType($format);

                $qrCode->save($qrCodePath);
               
            }

            $categoryitem = Categoryitem::where('id', '=', $undercategory)->get()->first();
          if (!isset($categoryitem->id)) {
                throw new \Exception('Brak załadowanej podkategorii');
            }           
            
           
            $product->files()->associate($_file);
            $product->categoryitem()->associate($categoryitem);
         
            $this->product->update($product, $request->all());
           
            DB::commit();
            Log::info('Poprawnie zaktualizowano produkt o  id : '.$product->id);
        } catch (\Exception $e) {
           // dd($e->getMessage());
                DB::rollback();
                Log::alert('Błąd przy aktualizacji : '.$product->id);
             return redirect()->route('admin.page.product.indexproducts')
                        ->withSuccess("Wystąpił błąd");
            
        }




        return redirect()->route('admin.page.product.indexproducts')
                        ->withSuccess(trans('page::messages.product.edit'));
    }

    public function indexproducts() {

         $products = $this->product->all();
         

        return view('page::admin.product-index', compact('products'));
    }

    public function deleteproducts(Product $product) {
        $this->product->destroy($product);

        return redirect()->route('admin.page.product.indexproducts')
                        ->withSuccess(trans('page::messages.product.delete'));
    }

    public function editproducts(Product $product) {

       
        $category = $this->getCategory();  
        $categoryitemid = $product->categoryitem_id;

        return view('page::admin.edit-product', compact('product', 'category', 'categoryitemid'));
    }

    public function createproducts() {

        $category = $this->getCategory();
        return view('page::admin.create-product', compact('category'));
    }

    public function getCategory() {
        $categories = $this->category->allTranslatedIn(App::getLocale());
        $category = $this->formatCategoryItem($categories);
        return $category;
    }

    public function storeproducts(CreateProductRequest $request) {

        $file = $request->file;

        $format = $request->get('format');
        if (is_null($format)) {
            throw new \Exception('Brak formatu pliku');
        }
        $destinationPath = 'assets/files';
        if (is_null($file)) {
            throw new \Exception('Brak załadowanego pliku');
        }
        $fileName = $file->getClientOriginalName();
        $fileName = urlencode($fileName);
        $filesize = $file->getSize();

        $file->move(
                $destinationPath, $fileName);


        $description = $request->get('description');
        $name = $request->get('name');
        $undercategory = $request->get('undercategory');

        $code_index = Str::random(32);
        $qrurl = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $destinationPath . '/' . $fileName;


        DB::beginTransaction();

        try {
            $_file = new File();
            $_file->filename = $fileName;
            $_file->path = $qrurl;
            $_file->extension = $format;
            $_file->filesize = $filesize;
            $_file->code_index = $code_index;
            $_file->save();

            $product = new Product();
            $product->name = $name;
            $product->description = $description;
          
            $categoryitem = Categoryitem::where('id', '=', $undercategory)->get()->first();
                       
            if (!isset($categoryitem->id)) {
                throw new \Exception('Brak załadowanej podkategorii');
            }           
 
            $product->categoryitem()->associate($categoryitem);
            $product->files()->associate($_file);
           
            $product->save();
            DB::commit();
        } catch (\Exception $e) {
            
          // dd($e->getMessage());
            DB::rollback();
             return redirect()->route('admin.page.product.indexproducts')
                        ->withSuccess("Wystąpił błąd");
        }

        $qrCode = new QrCode();
        $qrCode
                ->setText($qrurl)
                ->setSize(300)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setLabel('')
                ->setLabelFontSize(16)
                ->setImageType($format);

        $qrCode->save("assets/images/" . $code_index . "." . $format);


        return redirect()->route('admin.page.product.indexproducts')
                        ->withSuccess(trans('page::messages.product.add'));
    }

    public function showproducts(Request $request) {

        if (is_null($request->product)) {
            throw new \Exception('Brak id produktu');
        }

        $product = $this->product->find($request->product);
        if (is_null($product)) {
            throw new \Exception('Brak załadowanego produktu');
        }
        $extension = $product->files()->get()->first()->extension;
        if (is_null($extension)) {
            throw new \Exception('Brak zawartości kodu');
        }

        $code_index = $product->files()->get()->first()->code_index;
        return view('page::admin.show-product', compact('code_index', 'extension', 'product'));
    }

    private function formatCategoryItem($categoryies) {

        $_category_ = [];
        foreach ($categoryies as $category) {
            foreach ($category->categoryitems()->get() as $key => $categoryitems) {
                if ($categoryitems->title == null || $categoryitems->title == "root")
                    continue;
                $_category_[$category->name][$categoryitems->id] = $categoryitems->title;
            }
        }


        return $_category_;
    }

}
