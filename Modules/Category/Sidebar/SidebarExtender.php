<?php

namespace Modules\Category\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\User\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender {

    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth) {
        $this->auth = $auth;
    }

    /**
     * @param category $category
     *
     * @return category
     */
    public function extendWith(Menu $menu) {


        $menu->group(trans('page::pages.product.category'), function (Group $group) {

            $group->item(trans('page::pages.product.category'), function (Item $item) {
                $item->icon('fa fa-map-o');
                $item->weight(1);
                $item->route('admin.page.page.index');


                $item->item(trans('page::pages.product.category'), function (Item $item) {
                    $item->icon('fa fa-hdd-o');
                    $item->weight(0);

                    $item->route('admin.category.category.index');
                    $item->authorize(
                            $this->auth->hasAccess('page.pages.index')
                    );
                });
                $item->item(trans('page::pages.product.products'), function (Item $item) {
                    $item->icon('fa fa-laptop');
                    $item->weight(1);

                    $item->route('admin.page.product.indexproducts');
                    $item->authorize(
                            $this->auth->hasAccess('page.pages.index')
                    );
                });


                $item->authorize(
                        $this->auth->hasAccess('page.pages.index')
                );
            });
            $group->authorize(
                    $this->auth->hasAccess('page.*')
            );
        });

        return $menu;
    }

//    public function extendWithd(Menu $category)
//    {
//        $category->group(trans('core::sidebar.content'), function (Group $group) {
//            $group->weight(90);
//            $group->item(trans('category::category.title'), function (Item $item) {
//                $item->weight(3);
//                $item->icon('fa fa-share-alt');
//                $item->route('admin.category.category.index');
//                $item->authorize(
//                    $this->auth->hasAccess('category.categories.index')
//                );
//            });
//        });
//        
//        $group->item(trans('page::pages.title.contactpage'), function (Item $item) {
//                $item->icon('fa fa-file');
//                $item->weight(1);
//                $item->route('admin.page.page.index');
//
//
//                $item->item(trans('page::contact.contact.data'), function (Item $item) {
//                    $item->icon('fa fa-tags');
//                    $item->weight(0);
//
//                    $item->route('admin.page.contact.index');
//                    $item->authorize(
//                            $this->auth->hasAccess('page.pages.index')
//                    );
//                });
//
//
//                $item->authorize(
//                        $this->auth->hasAccess('page.pages.index')
//                );
//            });
//
//        return $category;
//    }
}
