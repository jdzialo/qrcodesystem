<?php

namespace Modules\Category\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Category\Entities\Category;
use Modules\Category\Http\Requests\CreateCategoryRequest;
use Modules\Category\Http\Requests\UpdateCategoryRequest;
use Modules\Category\Repositories\CategoryItemRepository;
use Modules\Category\Repositories\CategoryRepository;
use Modules\Category\Services\CategoryRenderer;

class CategoryController extends AdminBaseController {

    /**
     * @var CategoryRepository
     */
    private $category;

    /**
     * @var CategoryItemRepository
     */
    private $categoryItem;

    /**
     * @var CategoryRenderer
     */
    private $categoryRenderer;

    public function __construct(
    CategoryRepository $category, CategoryItemRepository $categoryItem, CategoryRenderer $categoryRenderer
    ) {
        parent::__construct();
        $this->category = $category;
        $this->categoryItem = $categoryItem;
        $this->categoryRenderer = $categoryRenderer;
    }

    public function index() {
        $categories = $this->category->all();

        return view('category::admin.categories.index', compact('categories'));
    }

    public function create() {
        return view('category::admin.categories.create');
    }

    public function store(CreateCategoryRequest $request) {
        $this->category->create($request->all());

        return redirect()->route('admin.category.category.index')
                        ->withSuccess(trans('category::messages.category created'));
    }

    public function edit($category) {
        $category = Category::findOrFail($category);
        $categoryItems = $this->categoryItem->allRootsForCategory($category->id);

        $categoryStructure = $this->categoryRenderer->renderForCategory($category->id, $categoryItems->nest());

        return view('category::admin.categories.edit', compact('category', 'categoryStructure'));
    }

    public function update($category, UpdateCategoryRequest $request) {
        $category = Category::findorfail($category);
        $this->category->update($category, $request->all());

        return redirect()->route('admin.category.category.index')
                        ->withSuccess(trans('category::messages.category updated'));
    }

    public function destroy($category) {
        $category = Category::findorfail($category);
        $this->category->destroy($category);

        return redirect()->route('admin.category.category.index')
                        ->withSuccess(trans('category::messages.category deleted'));
    }

}
