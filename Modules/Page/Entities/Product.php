<?php

namespace Modules\Page\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Media\Support\Traits\MediaRelation;

class Product extends Model{

    use Translatable, NamespacedEntity, MediaRelation;

    protected $table = 'category__products';
    public $translatedAttributes = [

        
        'title'
        
    ];
    protected $fillable = [
        'name',
        'description',           
      
      
        'files_id' ,
        'categoryitem_id'
        
    ];


     public function files()
    {
        return $this->belongsTo(File::class);
    }
   
     public function categoryitem()
    {
        return $this->belongsTo(\Modules\Category\Entities\Categoryitem::class);
    }
   
    protected static $entityNamespace = 'asgardcms/page';

}
