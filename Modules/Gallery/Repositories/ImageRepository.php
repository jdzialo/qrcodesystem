<?php

namespace Modules\Gallery\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface ImageRepository extends BaseRepository
{
}
