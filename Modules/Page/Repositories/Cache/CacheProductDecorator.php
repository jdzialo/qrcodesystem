<?php

namespace Modules\Page\Repositories\Cache;

use Modules\Core\Repositories\Cache\BaseCacheDecorator;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Repositories\ContactRepository;
use Modules\Page\Repositories\FileRepository;
use Modules\Page\Repositories\ProductRepository;

class CacheProductDecorator extends BaseCacheDecorator implements ProductRepository
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(ProductRepository $product)
    {
        parent::__construct();
        $this->entityName = 'products';
        $this->repository = $product;
    }





 
}
