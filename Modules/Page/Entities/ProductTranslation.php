<?php

namespace Modules\Page\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model {

    protected $table = 'category__products_translations';
    protected $fillable = [
        
        'title',
  
    ];

}