@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('atraction::atractions.title.create atraction') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.atraction.atraction.index') }}">{{ trans('atraction::atractions.title.atractions') }}</a></li>
        <li class="active">{{ trans('atraction::atractions.title.create atraction') }}</li>
    </ol>
@stop

@section('styles')
    {!! Theme::script('js/vendor/ckeditor/ckeditor.js') !!}
    <link href="{!! Module::asset('category:css/nestable.css') !!}" rel="stylesheet" type="text/css" />
@stop

@section('content')
    {!! Form::open(['route' => ['admin.atraction.atraction.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-lg-6">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('atraction::admin.atractions.partials.create-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <button class="btn btn-default btn-flat" name="button" type="reset">{{ trans('core::core.button.reset') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.atraction.atraction.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-lg-6">
            <div class="box box-primary">
                <div class="box-body">
                    
                    <div class="clearfix"></div>
                    <div class="category-tree">
                        {!! $categoryStructure !!}
                    </div>
                    <div class="clearfix"></div>
                    <hr />
                    
                    <div class="form-group">
                        {!! Form::label("status", 'Status:') !!}
                        <select name="status" id="status" class="form-control">
                            @foreach($_statuses as $id => $status)
                                <option value="{{ $id }}" {{ old('status', 0) == $id ? 'selected' : '' }}>{{ $status }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::label("template", trans('page::pages.form.template')) !!}
                        <select name="template" id="template" class="form-control">
                            <option value="small" {{ old('template', 0) == 'small' ? 'selected' : '' }}>Small</option>
                            <option value="big" {{ old('template', 0) == 'big' ? 'selected' : '' }}>Big</option>
                        </select>
                    </div>

                    <div class='form-group{{ $errors->has('visiting_time') ? ' has-error' : '' }}'>
                        {!! Form::label('visiting_time', trans('atraction::atractions.form.visiting_time')) !!}
                        <input type="number" id="visiting_time" class="form-control" name="visiting_time" value="" placeholder="[h]" required />
                        {!! $errors->first('VisitingTime', '<span class="help-block">:message</span>') !!}
                    </div>
                    
                    <div class="checkbox{{ $errors->has('kids') ? ' has-error' : '' }}">
                        <input type="hidden" name="kids" value="0" />
                        <label for="kids">
                            <input id="kids"
                                   name="kids"
                                   type="checkbox"
                                   class="flat-blue"
                                   value="1" />
                            {{ trans('atraction::atractions.form.kids') }}
                            {!! $errors->first('kids', '<span class="help-block">:message</span>') !!}
                        </label>
                    </div>

                    <hr />
                    
                    <div class="row">
                        <div class="col-xs-9">
                            <div class="form-group">
                                <label>{{ trans('atraction::atractions.form.select_place') }} <span class="fa fa-refresh refresh-places-list" title="Refresh places list"></span></label>
                                <select name="place_id[]" class="input-tags selectized" id="place_id" multiple="multiple" data-placeholder="{{ trans('atraction::atractions.form.select_place') }}" style="width: 100%;">
                                    @foreach($allPlaces as $place)
                                        <option value="{{ $place->id }}">
                                            <strong>{{ $place->name }}</strong> [<span>{{ $place->location }}</span>]
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <label>{{ trans('atraction::atractions.form.no_on_list') }}?</label>
                            <a href="{{ route('admin.place.place.create') }}" target="_blank" class="btn btn-warning">{{ trans('atraction::atractions.form.add_place') }}</a><br />
                        </div>
                    </div>

                    <div class="form-group {{ $errors->has("is_main") ? 'has-error' : '' }}">
                        <label>{{ trans('atraction::atractions.form.main_place') }}</label>
                        <select name="is_main" class="form-control" id="is_main" required>
                            
                        </select>
                        {!! $errors->first("is_main", '<span class="help-block">:message</span>') !!}
                    </div>
                    
                    <!--<div id="places-map" style="width: 100%; height: 500px;"></div>-->
                    
                    <hr />
                    
                    @mediaMultiple('gallery')
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCO4wl6mzPUyrqvn_TCvI0szCbGwu64PU&libraries=places" async defer></script>
@section('scripts')
<script src="{!! Module::asset('category:js/jquery.nestable.js') !!}"></script>
{{-- {!! Theme::script('js/map.js') !!} --}}
    <script type="text/javascript">
        
//        $('#places-map').mapify({
//            zoom: 9,
//            location: 'Rzeszów',
//            center: new google.maps.LatLng(50.0054088, 21.9184157)
//        });
        
        
          $('#place_id').selectize({
              delimiter: ',',
              persist: false,
              create: false,
              plugins: ['remove_button']
          });
          
        function dataProcess(json) {
            var data = $.map(json, function (el, i) {
                return [el];
            });
            return data;
        }
        
        $( document ).ready(function() {
            $('.dd').nestable();
            
            $('select#place_id').change(function(){
                var htmlContent = $(this).html();
                $('#is_main').html(htmlContent);
                $('#is_main').find('option').removeAttr('selected');
            });
            
            $('.refresh-places-list').click(function(){
                $.ajax({
                    url: "{{ route('admin.atraction.atraction.places.all.refresh') }}",
                    type: 'GET',
                    beforeSend: function () {
                        $('select#place_id').html('');
                    },
                    success: function (result) {
                        $('#place_id').selectize()[0].selectize.destroy();
                        $('select#place_id').html(result.placesHtml);
                        $('#place_id').selectize({
                            delimiter: ',',
                            persist: false,
                            create: false,
                            plugins: ['remove_button']
                        });
                        
                        $('#place_id').selectize({
                            delimiter: ',',
                            persist: false,
                            create: false,
                            plugins: ['remove_button']
                        });
                        $('#is_main').html('');
                    }
                });
            });
            
            
            
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.atraction.atraction.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@stop
