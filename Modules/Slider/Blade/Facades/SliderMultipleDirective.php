<?php

namespace Modules\Slider\Blade\Facades;

use Illuminate\Support\Facades\Facade;

class SliderMultipleDirective extends Facade {

    protected static function getFacadeAccessor() {
        return 'slider.multiple.directive';
    }

}
