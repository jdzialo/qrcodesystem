<?php

namespace Modules\Gallery\Repositories\Eloquent;

use Modules\Gallery\Repositories\GalleryRepository;
use Illuminate\Database\Eloquent\Builder;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Gallery\Events\GalleryWasCreated;
use Modules\Gallery\Events\GalleryWasDeleted;
use Modules\Gallery\Events\GalleryWasUpdated;


class EloquentGalleryRepository extends EloquentBaseRepository implements GalleryRepository
{
     /**
     * @param  mixed  $data
     * @return object
     */
    public function create($data) {
        $gallery = $this->model->create($data);

        event(new GalleryWasCreated($gallery->id, $data, $gallery));

//        $slider->setTags(array_get($data, 'tags', []));

        return $gallery;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data) {
       
        $model->update($data);
          
        event(new GalleryWasUpdated($model->id, $data, $model));

//        $model->setTags(array_get($data, 'tags', []));
        
        return $model;
    }

    public function destroy($gallery) {
//        $slider->untag();

        event(new GalleryWasDeleted($gallery));

        return $gallery->delete();
    }

    /**
     * @param $slug
     * @param $locale
     * @return object
     */
    public function findBySlugInLocale($slug, $locale) {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $q) use ($slug, $locale) {
                        $q->where('slug', $slug);
                        $q->where('locale', $locale);
                    })->with('translations')->first();
        }

        return $this->model->where('slug', $slug)->where('locale', $locale)->first();
    }
    public function allTranslatedIn($lang)
    {
        
        return $this->model->whereHas('translations', function (Builder $q) use ($lang) {
            $q->where('locale', "$lang");
         
        })->with('translations')->orderBy('created_at', 'DESC')->get();
    }
    
}
