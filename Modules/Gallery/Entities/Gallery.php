<?php

namespace Modules\Gallery\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Media\Support\Traits\MediaRelation;

class Gallery extends Model
{
    use Translatable , NamespacedEntity, MediaRelation;

    protected $table = 'gallery__galleries';
    public $translatedAttributes = [
        'title'
    ];
    protected $fillable = [
        'title'
    ];
     public function delete() {
        foreach ($this->files as $file) {
            $file->pivot->delete();
        }
        return parent::delete();
    }

	
    public function images() {
        return $this->hasMany('Modules\Gallery\Entities\Image')->orderBy('order', 'Asc');
    }

    protected static $entityNamespace = 'asgardcms/gallery';
}
