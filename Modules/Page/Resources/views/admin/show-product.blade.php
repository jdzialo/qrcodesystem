
@extends('layouts.master')

@section('content-header')


<div class="box-footer">
    
   
    <img src="{{"/assets/images/" .$code_index.".".$extension }}" alt="qr code">
        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.page.product.indexproducts')}}"><i class="fa fa-backward"></i> {{ trans('page::pages.product.return') }}</a>
        <a href="{{ URL::route('admin.page.product.qrdownload', [$product->id]) }}" style="margin-right: 10px" class="btn btn-default btn-flat pull-right"><i class=" fa fa-file-image-o"></i>{{ trans('page::pages.product.download') }}</a>
    </div>

@stop



