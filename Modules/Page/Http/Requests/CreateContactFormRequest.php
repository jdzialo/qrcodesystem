<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateContactFormRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
         
        'firstname' => 'required',
        'email' => 'required|email',
        'lastname' => 'required',
        'phone' => 'required|numeric',   
         'message' => 'required',
         'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'firstname.required' => trans('page::contact.firstname is required'),
            'lastname.required' => trans('page::contact.lastname is required'),
            'phone.required' => trans('page::contact.phone is required'),
            'email.required' => trans('page::contact.email is required'),
            'email.email' => trans('page::contact.email is not valid'),
            'phone.numeric' => trans('page::contact.phone is not valid'),
            'message.required' => trans('page::contact.message is required'),
            'g-recaptcha-response.required' => trans('page::contact.captcha is required')
        ];
    }
}
