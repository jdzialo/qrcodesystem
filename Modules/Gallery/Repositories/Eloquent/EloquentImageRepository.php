<?php

namespace Modules\Gallery\Repositories\Eloquent;

use Modules\Gallery\Repositories\ImageRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentImageRepository extends EloquentBaseRepository implements ImageRepository
{
    /**
       * @param  mixed  $data
     * @return object
     */
    public function create($data) {
        $gallery = $this->model->create($data);

        // event(new SliderWasCreated($slider->id, $data, $slider));

//        $slider->setTags(array_get($data, 'tags', []));

        return $gallery;
    }

    /**
     * @param $model
     * @param  array  $data
     * @return object
     */
    public function update($model, $data) {
        $model->update($data);

        // event(new SliderWasUpdated($model->id, $data, $model));

//        $model->setTags(array_get($data, 'tags', []));

        return $model;
    }

    public function destroy($gallery) {
//        $slider->untag();

        // event(new SliderWasDeleted($slider));

        return $gallery->delete();
    }

    /**
     * @param $slug
     * @param $locale
     * @return object
     */
    public function findBySlugInLocale($slug, $locale) {
        if (method_exists($this->model, 'translations')) {
            return $this->model->whereHas('translations', function (Builder $q) use ($slug, $locale) {
                        $q->where('slug', $slug);
                        $q->where('locale', $locale);
                    })->with('translations')->first();
        }

        return $this->model->where('slug', $slug)->where('locale', $locale)->first();
    }
}
