<?php

namespace Modules\Atraction\Repositories\Cache;

use Modules\Atraction\Repositories\AtractionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAtractionDecorator extends BaseCacheDecorator implements AtractionRepository {

    /**
     * @var PageRepository
     */
    protected $repository;
    
    public function __construct(AtractionRepository $atraction) {
        parent::__construct();
        $this->entityName = 'atraction.atractions';
        $this->repository = $atraction;
    }

}
