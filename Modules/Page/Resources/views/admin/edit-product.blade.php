@extends('layouts.master')

@section('content-header')


@stop

@section('styles')
{!! Theme::script('js/vendor/ckeditor/ckeditor.js') !!}
@stop

@section('content')
  <h3>{{ trans('page::pages.product.editproduct') }} <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </h3>
    {!! Form::open(['route' => ['admin.page.product.updateproducts',$product->id], 'method' => 'put' , 'files'=>true ]) !!}
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            
            @include('page::admin.partials.edit-product', ['lang' => locale(), 'errors' => $errors , 'product' => $product])
         </div> 
    </div>
</div>

{!! Form::close() !!}
@stop

@section('footer')
<a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
<dl class="dl-horizontal">
    <dt><code>b</code></dt>
    <dd>{{ trans('core::core.back to index') }}</dd>
</dl>
@stop

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).keypressAction({
            actions: [
                {key: 'b', route: "<?= route('admin.gallery.gallery.index') ?>"}
            ]
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue'
        });
    });
</script>
@stop
