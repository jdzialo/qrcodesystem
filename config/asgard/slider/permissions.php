<?php

return [
    'slider.sliders' => [
        'index' => 'slider::sliders.list resource',
        'create' => 'slider::sliders.create resource',
        'edit' => 'slider::sliders.edit resource',
        'destroy' => 'slider::sliders.destroy resource',
    ],
];
