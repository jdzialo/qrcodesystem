<?php

return [
    'title' => [
        'sliders' => 'Slider',
        'create slider' => 'Nowy Slider',
        'edit slider' => 'Edycja Slidera'
    ],
    'popup' => [
        'slide_content' => 'Opis slijdu',
        'title' => 'Tytuł',
        'content' => 'Opis',
        'url' => 'Link',
    ],
    'button' => [
        'create slider' => 'Nowy Slider' 
    ],
    'form' => [
        'title' => "Tytuł"
    ] ,
    
];