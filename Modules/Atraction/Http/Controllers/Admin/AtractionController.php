<?php

namespace Modules\Atraction\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Atraction\Entities\Atraction;
use Modules\Atraction\Repositories\AtractionRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use App\Status;
use Modules\Media\Repositories\FileRepository;
use Modules\Place\Entities\Place;
use Modules\Atraction\Http\Requests\CreateAtractionRequest;
use Modules\Atraction\Http\Requests\UpdateAtractionRequest;
use Modules\Category\Entities\Category;

use Modules\Category\Repositories\CategoryItemRepository;
use Modules\Category\Repositories\CategoryRepository;
use Modules\Category\Services\CategoryRenderer;

class AtractionController extends AdminBaseController {

    /**
     * @var AtractionRepository
     */
    private $atraction;
    
    /**
     * @var Status
     */
    private $status;
    
    /**
     * @var FileRepository
     */
    private $file;
    
    /**
     * @var CategoryRepository
     */
    private $category;

    /**
     * @var CategoryItemRepository
     */
    private $categoryItem;

    /**
     * @var CategoryRenderer
     */
    private $categoryRenderer;

//    public function __construct(AtractionRepository $atraction, Status $status, FileRepository $file) {
    public function __construct(AtractionRepository $atraction, Status $status, FileRepository $file, CategoryRepository $category, CategoryItemRepository $categoryItem, CategoryRenderer $categoryRenderer) {
        parent::__construct();

        $this->atraction = $atraction;
        $this->status = $status;
        $this->file = $file;
        
        $this->category = $category;
        $this->categoryItem = $categoryItem;
        $this->categoryRenderer = $categoryRenderer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $_statuses = $this->status->lists();
        $_statusClasses = $this->status->classes();
        
        $atractions = $this->atraction->all();

        return view('atraction::admin.atractions.index', compact('atractions', '_statuses', '_statusClasses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $_statuses = $this->status->lists();
        $allPlaces = Place::with(['translations'])->get();
        
        $category = Category::find(1)->first();
        $categoryItems = $this->categoryItem->allRootsForCategory($category->id);
        $categoryStructure = $this->categoryRenderer->renderTree($category->id, $categoryItems->nest());
        
        return view('atraction::admin.atractions.create', compact('_statuses', 'allPlaces', 'categoryStructure'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(CreateAtractionRequest $request) {
        $atraction = $this->atraction->create($request->all());
        
        foreach ($request->place_id as $place_id) {
            if ($place_id == $request->is_main) {
                $atraction->places()->attach([$place_id => ['is_main' => 1]]);
            }else{
                $atraction->places()->attach($place_id);
            }
        }
        
        $atraction->categories()->sync($request->categoryitems_ids);

        return redirect()->route('admin.atraction.atraction.index')
                        ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('atraction::atractions.title.atractions')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Atraction $atraction
     * @return Response
     */
    public function edit(Atraction $atraction) {
        $entityCategories = $atraction->categories()->get();
        $entityCategoriesIds = array();
        foreach ($entityCategories as $item) {
            $entityCategoriesIds[] = $item->id;
        }
        
        $category = Category::find(1)->first();
        $categoryItems = $this->categoryItem->allRootsForCategory($category->id);
        $categoryStructure = $this->categoryRenderer->renderTree($category->id, $categoryItems->nest(), $entityCategoriesIds);

        $_statuses = $this->status->lists();
        $entityPlaces = $atraction->places()->get();
        $restPlaces = Place::with(['translations'])->get()->diff($entityPlaces);

//        $coordinates = array();
//        foreach ($entityPlaces as $place) {
//            $coordinates[$place->id] = array($place->name, $place->coordinates);
//        }
//        $coordinates = json_encode($coordinates);
        
        return view('atraction::admin.atractions.edit', compact('atraction', '_statuses', 'entityPlaces', 'restPlaces', 'coordinates', 'categoryitems', 'categoryStructure'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  Atraction $atraction
     * @param  Request $request
     * @return Response
     */
    public function update(Atraction $atraction, UpdateAtractionRequest $request) {
        $this->atraction->update($atraction, $request->all());
        
//        $atraction->places()->sync($request->place_id); // working without setting is_main field
        $atraction->places()->sync([]);
        foreach ($request->place_id as $place_id) {
            if ($place_id == $request->is_main) {
                $atraction->places()->attach([$place_id => ['is_main' => 1]]);
            }else{
                $atraction->places()->attach($place_id);
            }
        }
        
        $atraction->categories()->sync($request->categoryitems_ids);
        
        return redirect()->route('admin.atraction.atraction.index')
                        ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('atraction::atractions.title.atractions')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Atraction $atraction
     * @return Response
     */
    public function destroy(Atraction $atraction) {
        $this->atraction->destroy($atraction);
        
        $atraction->places()->sync([]);

        return redirect()->route('admin.atraction.atraction.index')
                        ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('atraction::atractions.title.atractions')]));
    }
    
    public function refreshEntityPlaces(Atraction $atraction) {
        $entityPlaces = $atraction->places()->get();
        $restPlaces = Place::with(['translations'])->get()->diff($entityPlaces);
        
        $placesHtml = '';
        $entityPlacesHtml = '';
        foreach ($entityPlaces as $place) {
            $placesHtml .= '<option value="'.$place->id.'" selected>'.$place->name.' ['.$place->location.']</option>';
            if($place->pivot->is_main){
                $entityPlacesHtml .= '<option value="'.$place->id.'" selected>'.$place->name.' ['.$place->location.']</option>';
            }else{
                $entityPlacesHtml .= '<option value="'.$place->id.'">'.$place->name.' ['.$place->location.']</option>';
            }
        }

        foreach ($restPlaces as $place) {
            $placesHtml .= '<option value="'.$place->id.'">'.$place->name.' ['.$place->location.']</option>';
        }
        
        return response()->json([
            'placesHtml' => $placesHtml,
            'entityPlaces' => $entityPlacesHtml
        ]);
    }
    
    public function refreshAllPlaces() {
        $allPlaces = Place::with(['translations'])->get();
        
        $placesHtml = '';
        foreach ($allPlaces as $place) {
            $placesHtml .= '<option value="'.$place->id.'">'.$place->name.' ['.$place->location.']</option>';
        }
        
        return response()->json([
            'placesHtml' => $placesHtml
        ]);
    }

}
