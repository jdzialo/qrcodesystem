@extends('layouts.master')
@section('content')

@include('slider.my-own-slider')
<link>
 {!! Theme::style('vendor/magnific-popup/dist/magnific-popup.css') !!}
 </link>
<div class="container">
    <div class="row bottom-buffer">
        <div class="betweendiv">
            <div class="col-sm-6 divinformationleft">
                <h1 class="font700">
                    {{ trans('page::pages.home.title1') }}
                </h1>
                <p class="font700">
                    {{ trans('page::pages.home.section1') }}
                </p>
                <p>
                    {{ trans('page::pages.home.section2') }}
                </p>
            </div>
            <div class="col-sm-6 bannerIndex-1">

            </div>
            <div class="buttonbetween">
              <a class="popup-youtube" href="https://www.youtube.com/watch?v=7BxAL8qh9I8">  <div class="circle-button camera">    </div></a>
              
             
            </div>
        </div>
    </div>
</div>

@include('offer.offer')

<div class="container">
    <div class="row bottom-buffer">
        <div class="betweendiv">
            <div class="col-sm-6 bannerIndex-2">
            </div> 
            <div class="col-sm-6 divinformationright">
                <h1 class="font700">
                    {{ trans('page::pages.home.title2') }}
                </h1>
                <p class="font700">
                    {{ trans('page::pages.home.section3') }}
                </p>
                <p>
                    {{ trans('page::pages.home.section4') }}
                </p>
            </div>  
            <div class="buttonbetween">
                <!--<button type="submit" class="btn btn-default circle-button camera2"></button>-->
                <div class="circle-button camera2">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hidden-lg hidden-md">
    <div class="text-center bluecontainer bluemenu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    {{ trans('page::pages.home.bottomT1') }}
                    <div class="buttonposition">
                        <div class="circle-button buttonimg1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="more-information text-center">
                    <p>
                        Z rampą nastawną można przystosować wszelkie miejsca, które są trudne do pokonania przez wózek. Nadaje się do wszystkich pomieszczeń i do każdego podłoża.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <button type="submit" class="btn btn-default btn-blue">
                    Czytaj więcej <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;"></i>
                </button>
            </div>
            <div class="col-xs-3"></div>
        </div>
        <hr />
    </div>

    <div class="text-center bluecontainer bluemenu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    {{ trans('page::pages.home.bottomT2') }}
                    <div class="buttonposition">
                        <div class="circle-button buttonimg2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="more-information text-center">
                    <p>
                        Z rampą nastawną usprawnisz transport. Nadaje się do wykorzystania we wszystkich pomieszczeniach przemysłowych, halach produkcyjnych, magazynach, jak i instytucjach oraz miejscach użytku publicznego.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <button type="submit" class="btn btn-default btn-blue">
                    Czytaj więcej <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;"></i>
                </button>
            </div>
            <div class="col-xs-3"></div>
        </div>
        <hr />
    </div>

    <div class="text-center bluecontainer bluemenu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    {{ trans('page::pages.home.bottomT3') }}
                    <div class="buttonposition">
                        <div class="circle-button buttonimg3">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="more-information text-center">
                    <p>
                        Prosta w obsłudze i łatwa w montażu, w pełni zautomatyzowana i wygodna w użyciu. Innowacyjne rozwiązanie na rynku, zaprojektowane z myślą o wszystkich rodzajach wózków inwalidzkich.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <button type="submit" class="btn btn-default btn-blue">
                    Czytaj więcej <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;"></i>
                </button>
            </div>
            <div class="col-xs-3"></div>
        </div>
        <hr />
    </div>

    <div class="text-center bluecontainer bluemenu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    {{ trans('page::pages.home.bottomT4') }}
                    <div class="buttonposition">
                        <div class="circle-button buttonimg1">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="more-information text-center">
                    <p>
                        Rampa nadaje się do montażu zarówno w mieszkaniach i domach prywatnych, jak i instytucjach oraz  miejscach użytku publicznego – w biurach i zakładach pracy
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-6">
                <button type="submit" class="btn btn-default btn-blue">
                    Czytaj więcej <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;"></i>
                </button>
            </div>
            <div class="col-xs-3"></div>
        </div>
        <hr />
    </div>
</div>

<!-- Wersja od Konrada: -->
<div class="hidden-xs hidden-sm">

    <div class="container">
        <div class="row">
            <div class="text-center bluecontainer bluemenu">
                <div class="col-md-3">
                    Zastosowanie dla niepelnosprawnych
                    <div class="buttonposition">
                        <div class="circle-button buttonimg1">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    Zastosowanie dla<br /> przemyslu
                    <div class="buttonposition">
                        <div class="circle-button buttonimg2">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    Zastosowanie<br /> w mieszkaniach
                    <div class="buttonposition">
                        <div class="circle-button buttonimg3">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    Zastosowanie <br />w architekturze
                    <div class="buttonposition">
                        <div class="circle-button buttonimg4">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-3 padding55">
                <div class="more-information text-center">
                    <p>
                        Z rampą nastawną można przystosować wszelkie miejsca, które są trudne do pokonania przez wózek. Nadaje się do wszystkich pomieszczeń i do każdego podłoża.
                    </p>
                </div>
                 <a href="/{{trans('page::pages.routing.application-for-disable')}}">
                <button type="submit" class="btn btn-default btn-blue">
                  <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;"></i> Czytaj więcej
                </button>
                      </a> 
            </div>
            <div class="col-md-3">
                <div class="more-information text-center">
                    <p>
                        Z rampą nastawną usprawnisz transport. Nadaje się do wykorzystania we wszystkich pomieszczeniach przemysłowych, halach produkcyjnych, magazynach, jak i instytucjach oraz miejscach użytku publicznego.
                    </p>
                </div>
                <a href="/{{trans('page::pages.routing.application-for-industry')}}">
                <button type="submit" class="btn btn-default btn-blue">
                  <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;"></i> Czytaj więcej
                </button>
                      </a> 
            </div>
            <div class="col-md-3">
                <div class="more-information text-center">
                    <p>
                        Prosta w obsłudze i łatwa w montażu, w pełni zautomatyzowana i wygodna w użyciu. Innowacyjne rozwiązanie na rynku, zaprojektowane z myślą o wszystkich rodzajach wózków inwalidzkich.
                    </p>
                </div>
                 <a href="/{{trans('page::pages.routing.application-for-disable')}}">
                <button type="submit" class="btn btn-default btn-blue">
                  <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;"></i> Czytaj więcej
                </button>
                      </a> 
                
             
            </div>
            <div class="col-md-3">
                <div class="more-information text-center">
                    <p>
                        Rampa nadaje się do montażu zarówno w mieszkaniach i domach prywatnych, jak i instytucjach oraz  miejscach użytku publicznego – w biurach i zakładach pracy
                    </p>
                </div>
               <a href="/{{trans('page::pages.routing.application-for-architecture')}}">
                <button type="submit" class="btn btn-default btn-blue">
                  <i class="fa fa-arrow-right" aria-hidden="true" style="padding-left: 15px;"></i> Czytaj więcej
                </button>
                      </a> 
            </div>
        </div>
    </div>
</div>

<!--<div class="row">
    <h1>{{ $page->title }}</h1>
    {!! $page->body !!}
</div>-->

@stop

@push('scripts')
{!! Theme::script('vendor/magnific-popup/dist/jquery.magnific-popup.js') !!}
 <script>
     $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
     </script>
@endpush


 

 

