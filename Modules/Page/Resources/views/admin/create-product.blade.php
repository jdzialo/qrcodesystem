@extends('layouts.master')

@section('content')
<h3>{{ trans('page::pages.product.addproduct') }} <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </h3>
{!! Form::open(['route' => 'admin.page.product.store', 'method' => 'post' , 'files'=>true ]) !!}
@include('page::admin.partials.create-product', ['lang' => locale(), 'errors' => $errors])
{!! Form::close() !!}
@stop




