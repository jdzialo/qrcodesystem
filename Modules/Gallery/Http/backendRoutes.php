<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/gallery'], function (Router $router) {
    $router->bind('gallery', function ($id) {
        return app('Modules\Gallery\Repositories\GalleryRepository')->find($id);
    });
    $router->get('galleries', [
        'as' => 'admin.gallery.gallery.index',
        'uses' => 'GalleryController@index',
        'middleware' => 'can:gallery.galleries.index'
    ]);
    $router->get('galleries/create', [
        'as' => 'admin.gallery.gallery.create',
        'uses' => 'GalleryController@create',
        'middleware' => 'can:gallery.galleries.create'
    ]);
    $router->post('galleries', [
        'as' => 'admin.gallery.gallery.store',
        'uses' => 'GalleryController@store',
        'middleware' => 'can:gallery.galleries.create'
    ]);
    $router->get('galleries/{gallery}/edit', [
        'as' => 'admin.gallery.gallery.edit',
        'uses' => 'GalleryController@edit',
        'middleware' => 'can:gallery.galleries.edit'
    ]);
    $router->put('galleries/{gallery}', [
        'as' => 'admin.gallery.gallery.update',
        'uses' => 'GalleryController@update',
        'middleware' => 'can:gallery.galleries.edit'
    ]);
    $router->delete('galleries/{gallery}', [
        'as' => 'admin.gallery.gallery.destroy',
        'uses' => 'GalleryController@destroy',
        'middleware' => 'can:gallery.galleries.destroy'
    ]);
    $router->bind('image', function ($id) {
        return app('Modules\Gallery\Repositories\ImageRepository')->find($id);
    });
    $router->get('images', [
        'as' => 'admin.gallery.image.index',
        'uses' => 'ImageController@index',
        'middleware' => 'can:gallery.images.index'
    ]);
    $router->get('images/create', [
        'as' => 'admin.gallery.image.create',
        'uses' => 'ImageController@create',
        'middleware' => 'can:gallery.images.create'
    ]);
    $router->post('images', [
        'as' => 'admin.gallery.image.store',
        'uses' => 'ImageController@store',
        'middleware' => 'can:gallery.images.create'
    ]);
    $router->get('images/{image}/edit', [
        'as' => 'admin.gallery.image.edit',
        'uses' => 'ImageController@edit',
        'middleware' => 'can:gallery.images.edit'
    ]);
    $router->put('images/{image}', [
        'as' => 'admin.gallery.image.update',
        'uses' => 'ImageController@update',
        'middleware' => 'can:gallery.images.edit'
    ]);
    $router->delete('images/{image}', [
        'as' => 'admin.gallery.image.destroy',
        'uses' => 'ImageController@destroy',
        'middleware' => 'can:gallery.images.destroy'
    ]);
// append


});
