<?php

return [
    'form' => [
        'firstname' => 'имя',
        'lastname' => 'имя',
        'name' => 'Name',
        'email' => 'Электронная почта',
        'phone' => 'телефон',
        'message' => 'message'
    ],
    'button' =>
    [
        'send'=>'послать'
    ],
     'contact' =>
    [
        'data'=>'данные',
        'send contactform' => 'Wysłałeś formularze kontaktowy',
         'receive message' => 'You received a message from' ,
         'agree' => 'Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb niezbędnych do realizacji procesu rekrutacji (zgodnie z Ustawą z dnia 29.08.1997 roku o Ochronie Danych Osobowych; tekst jednolity: Dz. U. z 2002r. Nr 101, poz. 926 ze zm.)'  
 
    ],
    'mail' => [
        'subject' => "Formularz kontaktowy"
    ],
    'firstname is required' => 'имя Потребовалось',
    'lastname is required' => 'имя Потребовалось',
    'email is required' => 'Электронная почта Потребовалось',
    'phone is required' => 'телефон Потребовалось',
      'email is not valid' => 'учитывая неправильное Электронная почта',
     'phone is not valid' => 'Nr telefonu jest nieprawidłowy',
    'message is required' => 'Wiadomość jest wymagana',
    'captcha is required' => 'Captcha is required',
];
