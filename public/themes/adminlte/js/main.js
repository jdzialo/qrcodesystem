$(document).ready(function () {
    $('[data-slug="source"]').each(function(){
	    $(this).slug();
	});

    $(document).ajaxStart(function() { Pace.restart(); });

    Mousetrap.bind('f1', function() {
        window.open('https://asgardcms.com/docs', '_blank');
    });
    
    $(".load-modal").click(function () {
        loadModal($(this));
    });
    $(document).on('hidden.bs.modal','.modal', function(){
        closeModal($(this));
    });
    
});


/**
 * Function to showing bootstrap modals by ajax method.
 * Function get data-partial name, attributesArray and token from button attributes
 * and set it to the ajax request.
 * .load-modal Class executing the function
 * @param $this
 * @returns {object}
 */
//{{ route('dashboard.grid.reset')  }}
window.loadModal = function loadModal($this){
    var partial = $this.data('partial');
    var data = $this.data('attributes');
    var state = $this.data('state');
    
    $.ajax({
        url: '/backend/dashboard/popup',
        type: 'GET',
        data: {
            partial: partial,
            state: state,
            data: data
        },
        success: function (result) {
            $('body').append(result);
            var partialId = '#'+partial;
            $(partialId).modal('show');
        }
    });
}

/**
 * Function to removing modal after closing
 */
window.closeModal = function closeModal($this){
    $this.remove();
}
//# sourceMappingURL=main.js.map
