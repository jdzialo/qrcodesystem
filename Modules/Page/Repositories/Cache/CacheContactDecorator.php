<?php

namespace Modules\Page\Repositories\Cache;

use Modules\Core\Repositories\Cache\BaseCacheDecorator;
use Modules\Page\Repositories\PageRepository;
use Modules\Page\Repositories\ContactRepository;

class CacheContactDecorator extends BaseCacheDecorator implements ContactRepository
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(ContactRepository $contact)
    {
        parent::__construct();
        $this->entityName = 'contacts';
        $this->repository = $contact;
    }



    /**
     * Count all records
     * @return int
     */
    public function countAll()
    {
        return $this->cache
            ->tags([$this->entityName, 'global'])
            ->remember("{$this->locale}.{$this->entityName}.countAll", $this->cacheTime,
                function () {
                    return $this->repository->countAll();
                }
            );
    }

 
}
