<?php

return [
    'form' => [
        'firstname' => 'Imię',
        'lastname' => 'Nazwisko',
         'name' => 'Nazwa',
        'email' => 'E-mail',
        'phone' => 'Telefon',
        'message' => 'Wiadomość',
        'instalation_location' => 'Opis miejsca instalacji',
        'description' => 'opis',
        'category' => 'Kategorie',
        'format' => 'Rozszerzenie',
        'changefile' => "Zmień plik",
         "uploadfile" => "Wyślij plik" ,
        "fileupdatemessage" => "Załadowano wybrany plik"
    ],
    'button' =>
    [
        'send'=>'Wyślij'
    ],
     'contact' =>
    [
        'data'=>'Dane kontaktowe',
        'send contactform' => 'Wysłałeś formularze kontaktowy',
         'receive message' => 'Otrzymałeś wiadomość od' ,
         'agree' => 'Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb niezbędnych do realizacji procesu rekrutacji (zgodnie z Ustawą z dnia 29.08.1997 roku o Ochronie Danych Osobowych; tekst jednolity: Dz. U. z 2002r. Nr 101, poz. 926 ze zm.' , 
         'receive offer message' => 'Otrzymałeś wiadomość od' ,
          'send offer' => 'Wysłałeś oferte',
    
    ],
    'mail' => [
        'subject' => "Formularz kontaktowy",
         'offersubject' => "Oferta"
    ],
    'firstname is required' => 'Imie jest wymagane',
    'lastname is required' => 'Nazwisko jest wymagane',
    'email is required' => 'Adres e-mail jest wymagany',
    'phone is required' => 'Nr telefonu jest wymagany',
    'email is not valid' => 'Podano nieporpawny adres e-mail',
     'phone is not valid' => 'Nr telefonu jest nieprawidłowy',
    'message is required' => 'Wiadomość jest wymgana',
    'captcha is required' => 'Captcha is required',
    
];
