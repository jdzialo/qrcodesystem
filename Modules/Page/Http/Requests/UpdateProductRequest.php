<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateProductRequest extends BaseFormRequest {

    public function rules() {
        return [
            'name' => 'required',
            'name' => 'required',
            "file" => "mimes:pdf",
            'description' => 'required'
        ];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [
            'name.required' => "Proszę zamieścić nazwę",
            'description.required' => "Proszę zamieścić opis"
        ];
    }

}
