<?php

namespace Modules\Atraction\Sidebar;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\User\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender {

    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth) {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu) {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->weight(1);
            $group->item(trans('atraction::atractions.title.atractions'), function (Item $item) {
                $item->weight(1);
                $item->icon('fa fa-institution');
                $item->route('admin.atraction.atraction.index');
                $item->authorize(
                        $this->auth->hasAccess('atraction.atractions.index')
                );
            });
        });

        return $menu;
    }

}
