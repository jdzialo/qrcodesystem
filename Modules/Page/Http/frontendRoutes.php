<?php

use Illuminate\Routing\Router;

/** @var Router $router */
if (! App::runningInConsole()) {
 
    $router->get('/', [
        'uses' => 'PublicController@homepage',
        'as' => 'homepage',
        'middleware' => config('asgard.page.config.middleware'),
    ]);
    $router->get(trans('page::pages.routing.contact'), [
        'uses' => 'PublicController@contactpage',
        'as' => 'contact',
        //'middleware' => config('asgard.page.config.middleware'),
    ]);
   
    $router->post('contact/store', [
        'uses' => 'PublicController@storecontact',
        'as' => 'contactStore',
        //'middleware' => config('asgard.page.config.middleware'),
    ]);
//     $router->get('offersend', [
//        'uses' => 'PublicController@offertpage',
//        'as' => 'offerSend',
//        //'middleware' => config('asgard.page.config.middleware'),
//    ]);

     $router->post('offersend/store', [        
        'uses' => 'PublicController@storeoffer',
        'as' => 'offerStore',
        //'middleware' => config('asgard.page.config.middleware'),
    ]);
      $router->get(trans('page::pages.routing.gallery'), [
        //'as' => $locale . '.gallery',
        'uses' => 'PublicController@gallery',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
  
    $router->any('{uri}', [
        'uses' => 'PublicController@uri',
        'as' => 'page',
        'middleware' => config('asgard.page.config.middleware'),
    ])->where('uri', '.*');
      $router->get('/', [
        //'as' => $locale . '.gallery',
        'uses' => 'PublicController@homepage',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);

}
