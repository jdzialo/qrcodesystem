<?php

namespace Modules\Category\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCategoryRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required',
            'primary' => 'unique:category__categories',
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.required' => trans('category::validation.name is required'),
            'primary.unique' => trans('category::validation.only one primary category'),
        ];
    }
}
