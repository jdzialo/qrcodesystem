<div class="row">
    <div class="swiper-container swiper-container-h">
        <div class="swiper-wrapper">

            <div class="swiper-slide">
                <div class="col-sm-6 col-xs-12 sliderImage-1"></div>
                <div class="buttonslider">
                    <!--<button type="submit" class="btn btn-default circle-button arrow"></button>-->
                    <div class="circle-button arrow">
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12 bluecontainer sliderInfo">
                    <h1>
                        Nie potrzebna jest już pomoc osoby trzeciej!
                    </h1>
                    <div class="content-slider">
                       Jako pierwsi na rynku oferujemy innowacyjne rozwiązanie, czyli samodzielną w obsłudze rampę nastawną.  Rampa to polski innowacyjny produkt idealnie dostosowany do potrzeb osób niepełnosprawnych. W stanie spoczynku doskonale komponuje się z otoczeniem. Uruchamiana tylko na czas pokonania przeszkody pilotem, chipem, przyciskiem naściennym.
                    </div>
                </div>  
            </div>
      
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination swiper-pagination-h"></div>
    </div>
</div>


@push('scripts')
<script>
    var swiper = new Swiper('.swiper-container', {
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        loop: false,
        autoplay: 4000,
        spaceBetween: 30
    });
</script>
@endpush




<!-- Initialize Swiper -->
<!--
<script>
    var swiperH = new Swiper('.swiper-container-h', {
        pagination: '.swiper-pagination-h',
        paginationClickable: true,
        spaceBetween: 50
    });
    var swiperV = new Swiper('.swiper-container-v', {
        pagination: '.swiper-pagination-v',
        paginationClickable: true,
        direction: 'vertical',
        spaceBetween: 50
    });
</script>

<div class="swiper-container swiper-container-h">
    <div class="swiper-wrapper">
        <div class="swiper-slide">Horizontal Slide 1</div>
        <div class="swiper-slide">
            <div class="swiper-container swiper-container-v">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">Vertical Slide 1</div>
                    <div class="swiper-slide">Vertical Slide 2</div>
                    <div class="swiper-slide">Vertical Slide 3</div>
                    <div class="swiper-slide">Vertical Slide 4</div>
                    <div class="swiper-slide">Vertical Slide 5</div>
                </div>
                <div class="swiper-pagination swiper-pagination-v"></div>
            </div>
        </div>
        <div class="swiper-slide">Horizontal Slide 3</div>
        <div class="swiper-slide">Horizontal Slide 4</div>
    </div>
     Add Pagination 
    <div class="swiper-pagination swiper-pagination-h"></div>
</div>-->

<!--<div class="col-lg-6" style="min-height: 150px; background-color: #c0d2e0;">
    area for left slider
    <button class="circle-button">
        t
    </button>
</div>
<div class="col-lg-6" style="min-height: 150px; background-color: #90afc7;">
    area for right slider
</div>-->
