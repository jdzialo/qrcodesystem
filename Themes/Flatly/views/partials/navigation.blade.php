
<div class="container-fluid language ">
    <div class="container">
        <div class="choose-language pull-right">

            <div class="flags pull-right">

                <a href="{{URL::to('/pl')}}"><button class="language-pl">   </button></a>

                <a href="{{URL::to('/en')}}"> <button class="language-en">   </button></a>

                <a href="{{URL::to('/ru')}}"><button class="language-ru"> </button></a>

            </div>
        </div>
    </div>
</div>
<div class="container menuup">
    <div class="col-sm-3">                      

        <a href="../">
            <img class="img-responsive logo" src="/assets/images/logo.jpg" />
        </a>
 
    </div>
    <div class="col-sm-9 text-right">
    
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ URL::to('/') }}">@setting('core::site-name')</a>
                </div>
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                    @menu('Menu')
                </div>
            </div>
        </nav>
    </div>
</div>


@push('scripts')
{!! Theme::script('js/menu.js') !!}
@endpush

