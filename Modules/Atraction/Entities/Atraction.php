<?php

namespace Modules\Atraction\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Media\Support\Traits\MediaRelation;

class Atraction extends Model {

    use Translatable,
        MediaRelation;

    protected $table = 'atraction__atractions';
    public $translatedAttributes = [
        'title',
        'subtitle',
        'slug',
        'content_short',
        'content',
        'film',
        'content2',
//        'quotation1',
//        'quotation2',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
    ];
    protected $fillable = [
        'kids',
        'template',
        'visiting_time',
        'status',
        // Translatable fields
        'page_id',
        'title',
        'subtitle',
        'slug',
        'content_short',
        'content',
        'film',
        'content2',
//        'quotation1',
//        'quotation2',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
    ];
    protected static $entityNamespace = 'asgardcms/atraction';

    /**
     * Deleting files connections (from media__imageables table)
     * by deleting a main entity
     */
    public function delete() {
        foreach ($this->files as $file) {
            $file->pivot->delete();
        }
        return parent::delete();
    }

    public function places() {
        return $this->morphToMany('Modules\Place\Entities\Place', 'entity', 'place__place_entity_connections')->withPivot(['is_main']);
    }

    public function categories() {
        return $this->morphToMany('Modules\Category\Entities\Categoryitem', 'entity', 'category__categoryitem_entity_connections');
    }

    public function getThumbnailAttribute() {
        $thumbnail = $this->files()->where('zone', 'gallery')->first();

        if ($thumbnail === null) {
            $thumbnail = '/assets/img/default-thumbnail.jpg';
            return $thumbnail;
        }

        return $thumbnail;
    }

    public function getGalleryThumbnail() {
        $galleryThumbnail = $this->files()->where('zone', 'gallery')->get();
        return $galleryThumbnail;
    }

}
