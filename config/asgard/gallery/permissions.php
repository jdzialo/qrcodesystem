<?php

return [
    'slider.sliders' => [
        'index' => 'gallery::gallery.list resource',
        'create' => 'gallery::gallery.create resource',
        'edit' => 'gallery::gallery.edit resource',
        'destroy' => 'gallery::gallery.destroy resource',
    ],
];
