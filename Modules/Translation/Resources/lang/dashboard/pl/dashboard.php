<?php

return [
    'name' => 'Zaplecze',
    'edit grid' => 'Edytuj siatkę',
    'reset grid' => 'Resetuj siatkę',
    'save grid' => 'Zapisz siatkę',
    'add widget' => 'Dodaj widget',
    'add widget to dashboard' => 'Dodaj widget do zaplecza',
    'reset not needed' => 'Zaplecze nie potrzebuje resetowania',
    'dashboard reset' => 'Zaplecze zostatało zresetowane.',
    'list resource' => 'View dashboard',
    'edit resource' => 'Update Zaplecze',
    'reset resource' => 'Reset dashboard',
    'PostCount' => 'Liczba postów',
    'Categories' => 'Kategorie'
];
