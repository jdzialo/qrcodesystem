<?php

namespace Modules\Category\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Category\Events\CategoryItemWasCreated;
use Modules\Category\Repositories\CategoryItemRepository;
use Modules\Category\Entities\CategoryitemTranslation;

class EloquentCategoryItemRepository extends EloquentBaseRepository implements CategoryItemRepository
{
    public function create($data)
    {
        $categoryItem = $this->model->create($data);

        event(new CategoryItemWasCreated($categoryItem));

        return $categoryItem;
    }

    public function update($categoryItem, $data)
    {
        $categoryItem->update($data);

        return $categoryItem;
    }

    /**
     * Get online root elements
     *
     * @param  int    $categoryId
     * @return object
     */
    public function rootsForCategory($categoryId)
    {
        return $this->model->whereHas('translations', function (Builder $q) {
            $q->where('status', 1);
            $q->where('locale', App::getLocale());
        })->with('translations')->whereCategoryId($categoryId)->orderBy('position')->get();
    }

    /**
     * Get all root elements
     *
     * @param  int    $categoryId
     * @return object
     */
    public function allRootsForCategory($categoryId)
    {
        return $this->model->with('translations')->whereCategoryId($categoryId)->orderBy('parent_id')->orderBy('position')->get();
    }

    /**
     * Get Items to build routes
     *
     * @return Array
     */
    public function getForRoutes()
    {
        $categoryitems = DB::table('category__categories')
            ->select(
                'primary',
                'category__categoryitems.id',
                'category__categoryitems.parent_id',
                'category__categoryitems.module_name',
                'category__categoryitem_translations.uri',
                'category__categoryitem_translations.locale'
            )
            ->join('category__categoryitems', 'category__categories.id', '=', 'category__categoryitems.category_id')
            ->join('category__categoryitem_translations', 'category__categoryitems.id', '=', 'category__categoryitem_translations.categoryitem_id')
            ->where('uri', '!=', '')
            ->where('module_name', '!=', '')
            ->where('status', '=', 1)
            ->where('primary', '=', 1)
            ->orderBy('module_name')
            ->get();

        $categoryitemsArray = [];
        foreach ($categoryitems as $categoryitem) {
            $categoryitemsArray[$categoryitem->module_name][$categoryitem->locale] = $categoryitem->uri;
        }

        return $categoryitemsArray;
    }

    /**
     * Get the root category item for the given category id
     *
     * @param  int    $categoryId
     * @return object
     */
    public function getRootForCategory($categoryId)
    {
        return $this->model->with('translations')->where(['category_id' => $categoryId, 'is_root' => true])->firstOrFail();
    }

    /**
     * Return a complete tree for the given category id
     *
     * @param  int    $categoryId
     * @return object
     */
    public function getTreeForCategory($categoryId)
    {
        $items = $this->rootsForCategory($categoryId);

        return $items->nest();
    }

    /**
     * @param  string $uri
     * @param  string $locale
     * @return object
     */
    public function findByUriInLanguage($uri, $locale)
    {
        return $this->model->whereHas('translations', function (Builder $q) use ($locale, $uri) {
            $q->where('status', 1);
            $q->where('locale', $locale);
            $q->where('uri', $uri);
        })->with('translations')->first();
    }
    
    public function findByCategoryItemTranslationId($category_item_translation_id)
    {
         $categoryitemtranslation = CategoryitemTranslation::where('categoryitem_id', '=', $category_item_translation_id)->where('locale', '=', locale())->get()->first();
         return $categoryitemtranslation;
    }
}
