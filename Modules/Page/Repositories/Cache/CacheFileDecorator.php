<?php

namespace Modules\Page\Repositories\Cache;

use Modules\Core\Repositories\Cache\BaseCacheDecorator;
use Modules\Page\Repositories\PageRepository;

use \Modules\Page\Repositories\FileRepository;

class CacheFileDecorator extends BaseCacheDecorator implements FileRepository
{
    /**
     * @var PageRepository
     */
    protected $repository;

    public function __construct(FileRepository $file)
    {
        parent::__construct();
        $this->entityName = 'files';
        $this->repository = $file;
    }





 
}
