@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('page::contact.contact.data') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('page::pages.title.pages') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">           
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="data-table table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>{{ trans('page::contact.form.firstname') }}</th>
                             <th>{{ trans('page::contact.form.lastname') }}</th>
                            <th>{{ trans('page::contact.form.phone') }}</th>
                            <th>{{ trans('page::contact.form.email') }}</th>
                            <th>{{ trans('core::core.table.created at') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (isset($contacts)): ?>
                            
                        <?php foreach ($contacts as $contact): ?>
                           
                        <tr>
                            <td>
                                <a href="#">
                                    {{ $contact->id }}
                                </a>
                            </td>
                            <td>
                                <a href="#">
                                    {{ $contact->firstname }}
                                </a>
                            </td>
                             <td>
                                <a href="#">
                                    {{ $contact->lastname }}
                                </a>
                            </td>
                            <td>
                                <a href="#">
                                    {{ $contact->phone }}
                                </a>
                            </td>
                            <td>
                                <a href="#">
                                    {{ $contact->email }}
                                </a>
                            </td>
                            <td>
                                <a href="#">
                                    {{ $contact->created_at }}
                                </a>
                            </td>
                           
                        </tr>
                        <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>{{ trans('page::contact.form.firstname') }}</th>
                             <th>{{ trans('page::contact.form.lastname') }}</th>
                            <th>{{ trans('page::contact.form.phone') }}</th>
                            <th>{{ trans('page::contact.form.email') }}</th>
                            <th>{{ trans('core::core.table.created at') }}</th>
                        </tr>
                        </tfoot>
                    </table>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('page::pages.title.create page') }}</dd>
    </dl>
@stop

@section('scripts')
    <?php $locale = App::getLocale(); ?>
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.page.page.create') ?>" }
                ]
            });
        });
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@stop
