<?php

namespace Modules\Page\Entities;

use Illuminate\Database\Eloquent\Model;

class FileTranslation extends Model {

    protected $table = 'category__files_translations';
     protected $fillable = [
     'locale',
        'title',
        'email',
        'files_id'
    ];

}