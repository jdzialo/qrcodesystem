<?php

namespace Modules\Atraction\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;

use Modules\Atraction\Entities\Atraction;
use Modules\Atraction\Repositories\AtractionRepository;
use Modules\Category\Entities\Category;
use Illuminate\Support\Facades\Request;


class PublicController extends BasePublicController {

    /**
     * @var AtractionRepository
     */
    private $atraction;

    /**
     * @var Application
     */
    private $app;
    

    public function __construct(AtractionRepository $atraction, Application $app) {
        parent::__construct();
        $this->atraction = $atraction;
        $this->app = $app;
    }

    public function index(Request $request) {
        $atractions = Atraction::with(['translations', 'files'])->paginate(16);
        $categories = Category::with(['categoryitems', 'categoryitems.translations'])->find(1)->categoryitems->where('is_root', 0);

        $str = '';
        foreach ($atractions as $entity) {
            if (!$entity->places->isEmpty()) {
                foreach ($entity->places as $place) {
                    $coordinates = array();
                    $coordinates[] = explode(', ', $place->coordinates);
                    foreach ($coordinates as $k => $v) {
                        foreach ($v as $key => $value) {
                            $v[$key] = floatval($value);
                        }
                        $coordinates[$k] = $v;
                    }
                    $coordinates = reset($coordinates);
                    $str .= '{position:['.$coordinates[0].', '.$coordinates[1].'], icon: "/assets/img/pin.png"}, ';
                }
            }
        }

        $breadcrumbs = array(
            trans('messages.atractions.h1') => ''
        );
        return view('atraction.index', compact('atractions', 'breadcrumbs', 'categories', 'str'));
    }
    
    public function show($slug) {
        $entity = $this->atraction->findBySlug($slug);

        $str = '';
        if ($entity->template == 'big' && !$entity->places->isEmpty()) {
            foreach ($entity->places as $place) {
                $coordinates = array();
                $coordinates[] = explode(', ', $place->coordinates);
                foreach ($coordinates as $k => $v) {
                    foreach ($v as $key => $value) {
                        $v[$key] = floatval($value);
                    }
                    $coordinates[$k] = $v;
                }
                $coordinates = reset($coordinates);
                $str .= '{position:['.$coordinates[0].', '.$coordinates[1].'], icon: "/assets/img/pin.png"}, ';
            }
        }
        $similar = $this->atraction->all()->random(4);

        return view('atraction.show', compact('entity', 'str', 'similar'));
    }

    
    public function filter(Request $request) {

        $categoryitemsIds = $request::input('ids');
        if (!empty($categoryitemsIds)) {
            $atractions = Atraction::with(['translations', 'files', 'categories'])->whereHas('categories', function ($query) {
                    $query->whereIn('categoryitem_id', request()->input('ids'));
                })->paginate(16);
        }else{
            $atractions = Atraction::with(['translations', 'files'])->paginate(16);
        }
        
        return view('atraction.list', compact('atractions'));
        
    }
    

}
