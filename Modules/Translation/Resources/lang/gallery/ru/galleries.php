<?php

return [
    'title' => [
        'galleries' => 'Galeria',
        'create gallery' => 'Nowy Galeria',
        'edit gallery' => 'Edycja Galerii'
    ],
    'popup' => [
        'slide_content' => 'Opis slijdu',
        'title' => 'Tytuł',
        'content' => 'Opis',
        'url' => 'Link',
    ],
    'button' => [
        'create gallery' => 'Nowa Galeria' 
    ],
    'form' => [
        'title' => "Tytuł"
    ] ,
    
];
