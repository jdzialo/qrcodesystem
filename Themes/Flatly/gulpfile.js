var gulp = require("gulp");
var shell = require('gulp-shell');
var elixir = require('laravel-elixir');
var themeInfo = require('./theme.json');
elixir.config.assetsPath = './resources';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {

    /**
     * Compile less
     */
    mix.less([
        "main.less"
    ], 'assets/css/main.css')
    .copy(
        'assets',
        '../../public/themes/' + themeInfo.name.toLowerCase()
    );

    /**
     * Concat scripts
     */
    mix.scripts([
        '/vendor/jquery/dist/jquery.js',
        '/vendor/bootstrap/dist/js/bootstrap.min.js',
        '/js/scripts.js'
    ], null, 'resources')
	.copy('resources/vendor', '../../public/themes/' + themeInfo.name.toLowerCase() + '/vendor' );

    /**
     * Copy Bootstrap fonts
     */
    // mix.copy(
    //     'resources/vendor/bootstrap/fonts',
    //     'assets/fonts'
    // );

    /**
     * Copy Fontawesome fonts
     */
    // mix.copy(
    //     'resources/vendor/font-awesome/fonts',
    //     'assets/fonts'
    // );
});
