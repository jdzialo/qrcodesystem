<div class="box-body">
    <div class='form-group{{ $errors->has("name") ? ' has-error' : '' }}'>
        {!! Form::text("name", old("name"), ['class' => 'form-control', 'placeholder' => trans('page::contact.form.name')]) !!}
        {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
    </div>
   
    
     <div class='file-edit form-group{{ $errors->has("file") ? ' has-error' : '' }}'>
        <label for="files" class="btn btn-primary btn-flat">{{ trans('page::contact.form.uploadfile') }} <i class="fa fa-upload" aria-hidden="true"></i> </label>
        <input name="file" id="files" style="visibility:hidden;" type="file" /> <p id="uploadmessage"></p>
         {!! $errors->first("file", '<span class="help-block">:message</span>') !!}
    </div>
    
    <div class='form-group{{ $errors->has("description") ? ' has-error' : '' }}'>
        {!! Form::textarea("description", old("description"), ['class' => 'form-control',  'placeholder' => trans('page::contact.form.description')]) !!}
        {!! $errors->first("description", '<span class="help-block">:message</span>') !!}
    </div>
       
     
    
    <div class='form-group{{ $errors->has('undercategory') ? ' has-error' : '' }}'>
        {!! Form::label('undercategory', trans('page::contact.form.category')) !!}
            {{ Form::select('undercategory', $category ,null , ['class' => 'form-control'] )}}
      
        {!! $errors->first('undercategory', '<span class="help-block">:message</span>') !!}
    </div>
    

    
    <div class='form-group{{ $errors->has('format') ? ' has-error' : '' }}'>
        {!! Form::label('format', trans('page::contact.form.format')) !!}
        {!! Form::select('format', ['png' => 'png','jpeg' => 'jpeg','gif' => 'gif'], null, ['class' => 'form-control']) !!}
        {!! $errors->first('format', '<span class="help-block">:message</span>') !!}
    </div>
    
    <div class="box-footer">
        {!! Form::submit(trans('page::contact.button.send'), 
        array('class'=>'btn btn-primary')) !!}
       
        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.page.product.indexproducts')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
    </div>


</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#files').change(function () {
            $("#uploadmessage").html("{{trans('page::contact.form.fileupdatemessage') }}"+" " +$('#files').val() );
            
        });


        $(".extension-format").prop('disabled', true);


     
    });

</script>