<?php

namespace Modules\Page\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Media\Support\Traits\MediaRelation;

class File extends Model{

    use Translatable, NamespacedEntity, MediaRelation;

    protected $table = 'category__files';
    public $translatedAttributes = [
       
      
        
    ];
    protected $fillable = [
        'filename',
        'path',
        'extension',
        'code_index',
         'filesize'
    ];


    public function products() {
        return $this->hasOne('Modules\Category\Entities\Product');
    }
    
    protected static $entityNamespace = 'asgardcms/page';

}
