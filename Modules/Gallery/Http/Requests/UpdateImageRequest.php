<?php

namespace Modules\Gallery\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class UpdateImageRequest extends BaseFormRequest {

    protected $translationsAttributesKey = 'gallery::gallery.validation.attributes';

    public function rules() {
        return [
        ];
    }

    public function translationRules() {
        return [
           //TODO: make update validation
        ];
    }

    public function authorize() {
        return true;
    }

    public function messages() {
        return [
        ];
    }

    public function translationMessages() {
        return [
            'title.required' => trans('gallery::messages.title is required')
        ];
    }

}
