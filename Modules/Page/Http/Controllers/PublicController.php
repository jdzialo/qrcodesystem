<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Page\Entities\Page;
use Modules\Page\Repositories\PageRepository;
use Modules\Slider\Entities\Slider;
use Modules\Page\Http\Requests;
use Modules\Page\Repositories\ContactRepository;
use Modules\Page\Http\Requests\CreateOfferFormRequest;
use Modules\Gallery\Repositories\GalleryRepository;
use Modules\Slider\Repositories\SlideRepository;
use Illuminate\Support\Facades\App;

class PublicController extends BasePublicController {

    /**
     * @var PageRepository
     */
    private $page;

    /**
     * @var ContactRepository
     */
    private $contact;

    /**
     * @var Application
     */
    private $app;

    /**
     * @var Application
     */
    public $email;

    public function __construct(PageRepository $page, Application $app, ContactRepository $conctact, GalleryRepository $gallery, SlideRepository $slider) {
        parent::__construct();
        $this->page = $page;
        $this->app = $app;
        $this->contact = $conctact;
        $this->gallery = $gallery;
        $this->slider = $slider;
    }

    /**
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function uri($slug) {

        $page = $this->findPageForSlug($slug);

        $this->throw404IfNotFound($page);

        $template = $this->getTemplateForPage($page);

        $slides = $this->slider();

        if ($slides != null) {

            return view($template, compact('page', 'slides'));
        } else {
            return view($template, compact('page'));
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function homepage() {
        $page = $this->page->findHomepage();
        $this->throw404IfNotFound($page);      
        $template = $this->getTemplateForPage($page);
        $slides = $this->slider();
        if ($slides != null) {
            return view($template, compact('page', 'slides'));
        } else {
            return view($template, compact('page'));
        }
    }

    /**
     * Find a page for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Page
     */
    private function findPageForSlug($slug) {
        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());

        if ($menuItem) {
            return $this->page->find($menuItem->page_id);
        }

        return $this->page->findBySlug($slug);
    }

    /**
     * Return the template for the given page
     * or the default template if none found
     * @param $page
     * @return string
     */
    private function getTemplateForPage($page) {
        return (view()->exists($page->template)) ? $page->template : 'default';
    }

    /**
     * Throw a 404 error page if the given page is not found
     * @param $page
     */
    private function throw404IfNotFound($page) {
        if (is_null($page)) {
            $this->app->abort('404');
        }
    }

    public function contactpage() {
        return view('contact.index');
    }

    public function storecontact(Requests\CreateContactFormRequest $request) {
        $this->contact->create($request->all());
        $this->email = $request->get('email');
        \Mail::send('contact.email', array(
            'firstname' => $request->get('firstname'),
            'lastname' => $request->get('lastname'),
            'email' => $this->email,
            'contactMessage' => $request->get('message'),
                ), function($message) {
            $message->from($this->email);
            $message->to(env('MAIL_USERNAME'), 'Admin')->subject(trans('page::contact.mail.subject'));
        });

        return \Redirect::route('page', '/')
                        ->with('message', trans('page::contact.contact.send contactform'));
    }

    public function storeoffer(CreateOfferFormRequest $request) {
        $this->email = $request->get('email');
        \Mail::send('offer.email', array(
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'email' => $this->email,
            'instalation_location' => $request->get('instalation_location'),
                ), function($message) {
            $message->from($this->email);
            $message->to(env('MAIL_USERNAME'), 'Admin')->subject(trans('page::contact.mail.offersubject'));
        });

        return \Redirect::route('page', '/')
                        ->with('message', trans('page::contact.contact.send offer'));
    }

    public function gallery() {
        
        if ($this->gallery->allTranslatedIn(App::getLocale())->first() != null ) {
            $galleries = $this->gallery->allTranslatedIn(App::getLocale())->first()->files()->get();           
            return view('gallery.index', compact('galleries'));
        }
        return view('gallery.index');
    }

    public function slider() {
        if (Slider::with(['slides', 'slides.translations', 'slides.file'])->first() != null) {
            $slides = Slider::with(['slides', 'slides.translations', 'slides.file'])->first()->files()->get();
        }
       
        return $slides;
    }
    
    
}
