<?php

namespace Modules\Category\Entities;

use Illuminate\Database\Eloquent\Model;

class CategoryitemTranslation extends Model {

    public $fillable = ['title', 'uri', 'url', 'status', 'locale'];
    protected $table = 'category__categoryitem_translations';

    
//    public function products() {
//        return $this->hasOne('Modules\Page\Entities\Product');
//    }

}
