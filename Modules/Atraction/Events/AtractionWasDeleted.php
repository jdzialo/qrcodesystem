<?php

namespace Modules\Atraction\Events;

use Modules\Media\Contracts\DeletingMedia;

class AtractionWasDeleted implements DeletingMedia {

    /**
     * @var string
     */
    private $atractionClass;

    /**
     * @var int
     */
    private $atractionId;

    /**
     * @var object
     */
    public $atraction;

    public function __construct($atraction) {
        $this->atraction = $atraction;
        $this->atractionId = $atraction->id;
        $this->postClass = get_class($atraction);
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId() {
        return $this->atractionId;
    }

    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName() {
        return $this->atractionClass;
    }

}
