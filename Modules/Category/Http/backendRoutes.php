<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->bind('category', function ($id) {
    return app(\Modules\Category\Repositories\CategoryRepository::class)->find($id);
});
$router->bind('categoryitem', function ($id) {
    return app(\Modules\Category\Repositories\CategoryItemRepository::class)->find($id);
});

$router->group(['prefix' => '/category'], function (Router $router) {
    $router->get('categories', [
        'as' => 'admin.category.category.index',
        'uses' => 'CategoryController@index',
        'middleware' => 'can:category.categories.index',
    ]);
    $router->get('categories/create', [
        'as' => 'admin.category.category.create',
        'uses' => 'CategoryController@create',
        'middleware' => 'can:category.categories.create',
    ]);
    $router->post('categories', [
        'as' => 'admin.category.category.store',
        'uses' => 'CategoryController@store',
        'middleware' => 'can:category.categories.create',
    ]);
    $router->get('categories/{category_id}/edit', [
        'as' => 'admin.category.category.edit',
        'uses' => 'CategoryController@edit',
        'middleware' => 'can:category.categories.edit',
    ]);
    $router->put('categories/{category_id}', [
        'as' => 'admin.category.category.update',
        'uses' => 'CategoryController@update',
        'middleware' => 'can:category.categories.edit',
    ]);
    $router->delete('categories/{category_id}', [
        'as' => 'admin.category.category.destroy',
        'uses' => 'CategoryController@destroy',
        'middleware' => 'can:category.categories.destroy',
    ]);

    $router->get('categories/{category_id}/categoryitem', [
        'as' => 'dashboard.categoryitem.index',
        'uses' => 'CategoryItemController@index',
        'middleware' => 'can:category.categoryitems.index',
    ]);
    $router->get('categories/{category_id}/categoryitem/create', [
        'as' => 'dashboard.categoryitem.create',
        'uses' => 'CategoryItemController@create',
        'middleware' => 'can:category.categoryitems.create',
    ]);
    $router->post('categories/{category_id}/categoryitem', [
        'as' => 'dashboard.categoryitem.store',
        'uses' => 'CategoryItemController@store',
        'middleware' => 'can:category.categoryitems.create',
    ]);
    $router->get('categories/{category_id}/categoryitem/{categoryitem_id}/edit', [
        'as' => 'dashboard.categoryitem.edit',
        'uses' => 'CategoryItemController@edit',
        'middleware' => 'can:category.categoryitems.edit',
    ]);
    $router->put('categories/{category_id}/categoryitem/{categoryitem_id}', [
        'as' => 'dashboard.categoryitem.update',
        'uses' => 'CategoryItemController@update',
        'middleware' => 'can:category.categoryitems.edit',
    ]);
    $router->delete('categories/{category_id}/categoryitem/{categoryitem_id}', [
        'as' => 'dashboard.categoryitem.destroy',
        'uses' => 'CategoryItemController@destroy',
        'middleware' => 'can:category.categoryitems.destroy',
    ]);
    
});
