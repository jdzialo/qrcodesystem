<?php

namespace Modules\Page\Entities;

use Illuminate\Database\Eloquent\Model;

class ContactTranslation extends Model {

    protected $table = 'contact__contacts_translations';
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
    ];

}