@extends('layouts.master')

@section('content-header')
    <h1 class="pull-left">
        {{ trans('dashboard::dashboard.name') }}
    </h1>
    <div class="btn-group pull-right">
        <a class="btn btn-default" id="edit-grid" data-mode="0" href="#">{{ trans('dashboard::dashboard.edit grid') }}</a>
        <a class="btn btn-default" id="reset-grid" href="{{ route('dashboard.grid.reset')  }}">{{ trans('dashboard::dashboard.reset grid') }}</a>
        <a class="btn btn-default hidden" id="add-widget" data-toggle="modal" data-target="#myModal">{{ trans('dashboard::dashboard.add widget') }}</a>
    </div>
    <div class="clearfix"></div>
@stop

@section('styles')
    <style>
        .grid-stack-item {
            /*padding-right: 20px !important;*/
            width: 100% !important;
        }
    </style>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3> {{ $statistics['lastMonthCount'] }} <span class="no-bold" style="font-size: 14px;"> visits</span></h3>
                    <p>Durring last month</p>
                </div>
                <a href="javascript:void(0)" class="small-box-footer" style="opacity: 0;"><span>Products <i class="fa fa-arrow-circle-right"></i></span></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3> {{ $statistics['lastDayCount'] }} <span class="no-bold" style="font-size: 14px;"> visits</span></h3>
                    <p>Today</p>
                </div>
                <a href="javascript:void(0)" class="small-box-footer" style="opacity: 0;">Orders <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3> {{ $statistics['atractionCount'] }} <span class="no-bold" style="font-size: 14px;"> atractions</span></h3>
                    <p> In database</p>
                </div>
                <a href="javascript:void(0)" class="small-box-footer" style="opacity: 0;">News <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3> {{ $statistics['uniqueIpAddresses'] }} <span class="no-bold" style="font-size: 14px;"> unique visitors</span></h3>
                    <p> In last 6 months</p>
                </div>
                <a href="javascript:void(0)" class="small-box-footer" style="opacity: 0;">{{ trans('dashboard::messages.more_info') }} <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Visits</h3>
                </div>
                <div class="box-body">
                    <div id="bar-chart" style="height: 450px;"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="grid-stack">
            </div>
            <div class="clearfix"></div>
        </div>
<!--        <div class="col-lg-6">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Most visiting cities</h3>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
                </div>
            </div>
        </div>-->
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ trans('dashboard::dashboard.add widget to dashboard') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    </div>
                </div>
            </div>
        </div>
    </div>
            
@stop

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.categories.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    @parent
    <script type="text/javascript">
        $(document).ready(function () {
            
            var bar_data = {
              data: {!! $statistics['perMonth'] !!},
              color: "#3c8dbc"
            };
            $.plot("#bar-chart", [bar_data], {
              grid: {
                borderWidth: 1,
                borderColor: "#f3f3f3",
                tickColor: "#f3f3f3"
              },
              series: {
                bars: {
                  show: true,
                  barWidth: 0.5,
                  align: "center"
                }
              },
              xaxis: {
                mode: "categories",
                tickLength: 0
              }
            });
            
            
//            var donut = new Morris.Donut({
//              element: 'sales-chart',
//              resize: true,
//              colors: ["#3c8dbc", "#f56954", "#00a65a"],
//              data: [
//                {label: "Download Sales", value: 12},
//                {label: "In-Store Sales", value: 30},
//                {label: "Mail-Order Sales", value: 20}
//              ],
//              hideHover: 'auto'
//            });

            
            var options = {
                vertical_margin: 10,
                float: true,
                animate: true
            };
            $('.grid-stack').gridstack(options);

            /** savey crap */
            new function () {
                this.defaultWidgets = {!! json_encode($widgets) !!};
                this.serialized_data = {!! $customWidgets !== 'null' ? $customWidgets : json_encode($widgets) !!};
                //console.log(this.defaultWidgets.PostsWidget);
                this.grid = jQuery('.grid-stack').data('gridstack');
                this.load_grid = function () {
                    this.grid.remove_all();
                    var items = GridStackUI.Utils.sort(this.serialized_data);
                    _.each(items, function (node) {
                        this.spawn_widget(node);
                        jQuery(jQuery.find('option[value="'+node.id+'"]')[0]).hide();
                    }, this);
                }.bind(this);
                this.save_grid = function () {
                    this.serialized_data = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
                        el = jQuery(el);
                        var node = el.data('_gridstack_node');
                        return {
                            id: el.attr('id'),
                            x: node.x,
                            y: node.y,
                            width: node.width,
                            height: node.height
                        };
                    }, this);
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('dashboard.grid.save') }}',
                        data: {
                            _token: '<?= csrf_token() ?>',
                            grid: JSON.stringify(this.serialized_data)
                        },
                        success: function(data) {
                            console.log(data);
                        }
                    });
                }.bind(this);
                this.clear_grid = function () {
                    this.grid.remove_all();
                    jQuery(jQuery.find('option:hidden')).show();
                }.bind(this);
                this.edit_grid = function () {
                    mode = jQuery('#edit-grid').data('mode');
                    if (mode == 0) {
                        // enable all the grid editing
                        _.map(jQuery('.grid-stack > .grid-stack-item:visible'), function (el) {
                            this.grid.movable(el, true);
                            jQuery(el).on('dblclick', function (e) {
                                this.grid.resizable(el, true);
                            }.bind(this));
                        }, this);
                        jQuery('#edit-grid').data('mode', 1).text('{{ trans('dashboard::dashboard.save grid') }}');
                    } else {
                        // disable all the grid editing
                        _.map(jQuery('.grid-stack > .grid-stack-item:visible'), function (el) {
                            this.grid.movable(el, false);
                            this.grid.resizable(el, false);
                            jQuery(el).off('dblclick');
                        }, this);
                        jQuery('#edit-grid').data('mode', 0).text('{{ trans('dashboard::dashboard.edit grid') }}');
                        // run the save mech
                        this.save_grid();
                    }
                }.bind(this);
                this.spawn_widget = function (node) {
                    var html = node.html === undefined ? this.defaultWidgets[node.id].html : node.html,
                        element = jQuery('<div><div class="grid-stack-item-content" />' + html + '<div/>'),
                        x = node.options === undefined ? node.x : node.options.x,
                        y = node.options === undefined ? node.y : node.options.y,
                        width = node.options === undefined ? node.width : node.options.width,
                        height = node.options === undefined ? node.height : node.options.height;

                    this.grid.add_widget(element, x, y, width, height);

                    element.attr({id: node.id});
                    this.grid.resizable(element, false);
                    this.grid.movable(element, false);
                    return element;
                }.bind(this);
                jQuery('#edit-grid').on('click', this.edit_grid);
                jQuery('#myModal').on('hidden.bs.modal', function (e) {
                    value = jQuery('select[name=widget]').val();
                    if (value == 'x') {
                        return;
                    }
                    element = this.spawn_widget({
                        auto_position: true,
                        width: 12,
                        height: 2,
                        id: value
                    });
                    this.grid.resizable(element, true);
                    this.grid.movable(element, true);
                }.bind(this));
                this.load_grid();
            };

        });
    </script>
@stop