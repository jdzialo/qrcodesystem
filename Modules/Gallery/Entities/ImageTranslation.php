<?php

namespace Modules\Gallery\Entities;

use Illuminate\Database\Eloquent\Model;

class ImageTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'title',
        'content',
        'url'
    ];
    protected $table = 'gallery__image_translations';
}
