<?php

namespace Modules\Page\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Core\Traits\NamespacedEntity;
use Modules\Media\Support\Traits\MediaRelation;

class Contact extends Model{

    use Translatable, NamespacedEntity, MediaRelation;

    protected $table = 'contact__contacts';
    public $translatedAttributes = [
        'firstname',
        'lastname',
        'email',
        'phone',
        
    ];
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
    ];



	
   
    protected static $entityNamespace = 'asgardcms/page';

}
