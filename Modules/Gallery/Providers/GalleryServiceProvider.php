<?php

namespace Modules\Gallery\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Media\Image\ThumbnailManager;
use Modules\Gallery\Blade\GalleryMultipleDirective;

class GalleryServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
         $this->app->bind('gallery.multiple.directive', function () {
            return new GalleryMultipleDirective();
        });
    }

    public function boot()
    {
        $this->publishConfig('gallery', 'permissions');
        $this->registerBladeTags();
        $this->registerThumbnails();
    
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(FinderService::class, function () {
            return new FinderService();
        });
        $this->app->bind(
            'Modules\Gallery\Repositories\GalleryRepository',
            function () {
                $repository = new \Modules\Gallery\Repositories\Eloquent\EloquentGalleryRepository(new \Modules\Gallery\Entities\Gallery());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Gallery\Repositories\Cache\CacheGalleryDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Gallery\Repositories\ImageRepository',
            function () {
                $repository = new \Modules\Gallery\Repositories\Eloquent\EloquentImageRepository(new \Modules\Gallery\Entities\Image());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Gallery\Repositories\Cache\CacheImageDecorator($repository);
            }
        );
// add bindings


    }
    private function registerBladeTags() {
//        if (app()->environment() === 'testing') {
//            return;
//        }
        $this->app['blade.compiler']->directive('galleryMultiple', function ($value) {
            return "<?php echo GalleryMultipleDirective::show([$value]); ?>";
        });
    }
    
    private function registerThumbnails() {
        $this->app[ThumbnailManager::class]->registerThumbnail('partnerThumb', [
            'fit' => [
                'width' => '215',
                'height' => '150',
                'callback' => function ($constraint) {
                    $constraint->upsize();
                },
            ],
        ]);
    }
}
