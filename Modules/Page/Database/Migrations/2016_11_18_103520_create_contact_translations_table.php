<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTranslationsTable extends Migration
{
  /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contact__contacts_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('firstname');
            $table->string('lastname');
            $table->string('phone');
            $table->string('email'); 
            $table->text('message'); 
            
            $table->integer('contact_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['contact_id', 'locale']);
            $table->foreign('contact_id')->references('id')->on('contact__contacts')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('contact__contacts_translations', function (Blueprint $table) {
            $table->dropForeign(['contact_id']);
        });
        Schema::dropIfExists('contact__contacts_translations');
    }
}
