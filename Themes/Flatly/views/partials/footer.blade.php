<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                Wszystkie prawa zastrzeżone dla: profund.com.pl<br />
                Projekt i realizacja strony: moonbite.pl
            </div>
            <div class="col-sm-6 col-xs-12">
                {!! Menu::get('Footer') !!}
            </div>
        </div>
    </div>
</div>

<div class="footer-advertisement">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-6">
                <img class="img-responsive" src="/assets/images/kapital_ludzki.gif" />
            </div>
            <div class="col-sm-6 col-xs-6">
                <img class="img-responsive pull-right" src="/assets/images/ue.gif" />
            </div>
        </div>
    </div>
</div>