<?php

return [
     'product' => [
        'products' => "Products",
        'name' => "Name",
        'description' => "Description",
        "categoryname" => "Category",
        "undercategoryname" => "Undercategory",
        "new" => " New " ,
         "category" => "Categories" ,
        "editproduct" => "Edit",
        "addproduct" => "Dodaj",
        "return" => "Return",
        "download" => " Download "
        
    ],
    'routing' => [
        'gallery' => 'gallery',
         'contact' => 'contact',
        'application-for-disable' => 'application-for-disable',
        'application-for-industry' => 'application-for-industry',
        'application-for-architecture' => 'application-for-architecture'
        
    ],
    'title' => [
        'pages' => 'Pages',
        'create page' => 'Create a page',
        'edit page' => 'Edit a page',
        'contactpage' => 'Wiadomości kontaktowe'
    ],
    'button' => [
        'create page' => 'Create a page',
    ],
    'table' => [
        'name' => 'Name',
        'slug' => 'Slug',
    ],
    'form' => [
        'title' => 'Title',
        'slug' => 'Slug',
        'meta_data' => 'Meta data',
        'meta_title' => 'Meta title',
        'meta_description' => 'Meta description',
        'facebook_data' => 'Facebook data',
        'og_title' => 'Facebook title',
        'og_description' => 'Facebook description',
        'og_type' => 'Facebook type',
        'template' => 'Page template name',
        'is homepage' => 'Homepage ?',
        'body' => 'Body',
    ],
    'validation' => [
        'attributes' => [
            'title' => 'title',
            'body' => 'body',
        ],
    ],
    'facebook-types' => [
        'website' => 'Website',
        'product' => 'Product',
        'article' => 'Article',
    ],
    'navigation' => [
        'back to index' => 'Go back to the pages index',
    ],
    'list resource' => 'List pages',
    'create resource' => 'Create pages',
    'edit resource' => 'Edit pages',
    'destroy resource' => 'Delete pages',
];
