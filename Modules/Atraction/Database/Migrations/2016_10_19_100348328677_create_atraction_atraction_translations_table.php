<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtractionAtractionTranslationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('atraction__atraction_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            
            $table->string('title');
            $table->string('slug');
            $table->string('subtitle')->nullable();
            $table->string('content_short')->nullable();
            $table->text('content')->nullable();
            $table->string('film')->nullable();
            $table->text('content2')->nullable();
//            $table->string('quotation1')->nullable();
//            $table->string('quotation2')->nullable();
            
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();

            $table->string('og_title')->nullable();
            $table->string('og_description')->nullable();
            $table->string('og_image')->nullable();
            $table->string('og_type')->nullable();

            $table->integer('atraction_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['atraction_id', 'locale']);
            $table->foreign('atraction_id')->references('id')->on('atraction__atractions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('atraction__atraction_translations', function (Blueprint $table) {
            $table->dropForeign(['atraction_id']);
        });
        Schema::dropIfExists('atraction__atraction_translations');
    }

}
