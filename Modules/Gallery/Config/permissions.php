<?php

return [
    'gallery.galleries' => [
        'index' => 'gallery::galleries.list resource',
        'create' => 'gallery::galleries.create resource',
        'edit' => 'gallery::galleries.edit resource',
        'destroy' => 'gallery::galleries.destroy resource',
    ],
    'gallery.images' => [
        'index' => 'gallery::images.list resource',
        'create' => 'gallery::images.create resource',
        'edit' => 'gallery::images.edit resource',
        'destroy' => 'gallery::images.destroy resource',
    ],
// append


];
