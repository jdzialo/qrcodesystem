<?php

namespace Modules\Category\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use TypiCMS\NestableTrait;

class Categoryitem extends Model
{
    use Translatable, NestableTrait;

    public $translatedAttributes = ['title', 'uri', 'url', 'status', 'locale'];
    protected $fillable = [
        'category_id',
        'page_id',
        'parent_id',
        'position',
        'target',
        'module_name',
        'title',
        'uri',
        'url',
        'status',
        'is_root',
        'icon',
        'link_type',
        'locale',
        'class',
    ];
    protected $table = 'category__categoryitems';

    /**
     * For nested collection
     *
     * @var array
     */
    public $children = [];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    
      public function products() {
        return $this->hasOne('Modules\Category\Entities\Product');
    }
    /**
     * Make the current category item child of the given root item
     * @param Categoryitem $rootItem
     */
    public function makeChildOf(Categoryitem $rootItem)
    {
        $this->parent_id = $rootItem->id;
        $this->save();
    }

    /**
     * Check if the current category item is the root
     * @return bool
     */
    public function isRoot()
    {
        return (bool) $this->is_root;
    }

    /**
     * Check if page_id is empty and returning null instead empty string
     * @return number
     */
    public function setPageIdAttribute($value)
    {
        $this->attributes['page_id'] = ! empty($value) ? $value : null;
    }

    /**
     * Check if parent_id is empty and returning null instead empty string
     * @return number
     */
    public function setParentIdAttribute($value)
    {
        $this->attributes['parent_id'] = ! empty($value) ? $value : null;
    }
}
