<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryImageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery__image_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');          
            $table->integer('image_id')->unsigned();
            $table->string('locale')->index();            
            $table->string('title');
            $table->string('content');
            $table->string('url');
            
            $table->unique(['image_id', 'locale']);
            $table->foreign('image_id')->references('id')->on('gallery__images')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gallery__image_translations', function (Blueprint $table) {
            $table->dropForeign(['image_id']);
        });
        Schema::dropIfExists('gallery__image_translations');
    }
}
