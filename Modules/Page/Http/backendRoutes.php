<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->bind('page', function ($id) {
    return app(\Modules\Page\Repositories\PageRepository::class)->find($id);
});

$router->group(['prefix' => '/page'], function (Router $router) {
    $router->get('pages', [
        'as' => 'admin.page.page.index',
        'uses' => 'PageController@index',
        'middleware' => 'can:page.pages.index',
    ]);
    $router->get('contactform', [
        'as' => 'admin.page.contact.index',
        'uses' => 'PageController@show',
            // 'middleware' => 'can:page.pages.index',
    ]);
    $router->get('pages/create', [
        'as' => 'admin.page.page.create',
        'uses' => 'PageController@create',
        'middleware' => 'can:page.pages.create',
    ]);
    $router->post('pages', [
        'as' => 'admin.page.page.store',
        'uses' => 'PageController@store',
        'middleware' => 'can:page.pages.create',
    ]);
    $router->get('pages/{page}/edit', [
        'as' => 'admin.page.page.edit',
        'uses' => 'PageController@edit',
        'middleware' => 'can:page.pages.edit',
    ]);
    $router->put('pages/{page}/edit', [
        'as' => 'admin.page.page.update',
        'uses' => 'PageController@update',
        'middleware' => 'can:page.pages.edit',
    ]);
    $router->delete('pages/{page}', [
        'as' => 'admin.page.page.destroy',
        'uses' => 'PageController@destroy',
        'middleware' => 'can:page.pages.destroy',
    ]);


    $router->get('createproduct', [
        'as' => 'admin.page.product.createproducts',
        'uses' => 'PageController@createproducts',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);

    $router->get('product/{product}/edit', [
        'as' => 'admin.page.product.editproducts',
        'uses' => 'PageController@editproducts',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
     $router->put('product/{product}/edit', [
        'as' => 'admin.page.product.updateproducts',
        'uses' => 'PageController@updateproducts',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
    $router->delete('product/{product}/delete', [
        'as' => 'admin.page.product.deleteproducts',
        'uses' => 'PageController@deleteproducts',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);

    $router->post('product/store', [
        'as' => 'admin.page.product.store',
        'uses' => 'PageController@storeproducts',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
 

    $router->get('qrcode/index', [
        'as' => 'admin.page.product.indexproducts',
        'uses' => 'PageController@indexproducts',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
    $router->get('qrcode/{product}/show', [
        'as' => 'admin.page.product.showproducts',
        'uses' => 'PageController@showproducts',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
      $router->get('qrcode/{product}/download', [
        'as' => 'admin.page.product.qrdownload',
        'uses' => 'PageController@downloadqrcode',
//        'middleware' => config('asgard.blog.config.middleware'),
    ]);
  //downloadQrCode
  //
  //
//     
});
