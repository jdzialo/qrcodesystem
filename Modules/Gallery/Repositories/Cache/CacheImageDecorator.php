<?php

namespace Modules\Gallery\Repositories\Cache;

use Modules\Gallery\Repositories\ImageRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheImageDecorator extends BaseCacheDecorator implements ImageRepository
{
    public function __construct(ImageRepository $image)
    {
        parent::__construct();
        $this->entityName = 'gallery.images';
        $this->repository = $image;
    }
}
