<?php

namespace Modules\Atraction\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Media\Image\ThumbnailManager;

class AtractionServiceProvider extends ServiceProvider {

    use CanPublishConfiguration;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->registerBindings();
    }

    public function boot() {
        $this->publishConfig('atraction', 'permissions');
        
        $this->registerThumbnails();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array();
    }

    private function registerBindings() {
        $this->app->bind(
                'Modules\Atraction\Repositories\AtractionRepository', function () {
            $repository = new \Modules\Atraction\Repositories\Eloquent\EloquentAtractionRepository(new \Modules\Atraction\Entities\Atraction());

            if (!config('app.cache')) {
                return $repository;
            }

            return new \Modules\Atraction\Repositories\Cache\CacheAtractionDecorator($repository);
        }
        );
    }
    
    
    private function registerThumbnails() {
        $this->app[ThumbnailManager::class]->registerThumbnail('artactionThumb', [
            'fit' => [
                'width' => '300',
                'height' => '300',
                'callback' => function ($constraint) {
                    $constraint->upsize();
                },
            ],
        ]);
//        $this->app[ThumbnailManager::class]->registerThumbnail('295x175', [
//            'fit' => [
//                'width' => '295',
//                'height' => '175',
//                'callback' => function ($constraint) {
//                    $constraint->upsize();
//                },
//            ],
//        ]);
        $this->app[ThumbnailManager::class]->registerThumbnail('fb_thumb', [
            'fit' => [
                'width' => '1200',
                'height' => '630',
                'callback' => function ($constraint) {
                    $constraint->upsize();
                },
            ],
        ]);
        $this->app[ThumbnailManager::class]->registerThumbnail('main_photo', [
            'fit' => [
                'width' => '1920',
                'height' => '780',
                'callback' => function ($constraint) {
                    $constraint->upsize();
                },
            ],
        ]);
    }

}
