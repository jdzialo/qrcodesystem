<?php

return [
    /* Menu management */
    'menu created' => 'Menu successfully created.',
    'menu not found' => 'Menu not found.',
    'menu updated' => 'Menu successfully updated.',
    'menu deleted' => 'Menu successfully deleted.',
    /* MenuItem management */
    'menuitem created' => 'Menu item successfully created.',
    'menuitem not found' => 'Menu item not found.',
    'menuitem updated' => 'Menu item successfully updated.',
    'menuitem deleted' => 'Menu item successfully deleted.',
    'category created' => 'Created category',
    'categoryitem created' => 'Createditem category',
    
    'category not found' => 'Category not found',
    'category updated' => 'Category was updated',
    'category deleted' => 'Category was deleted',
    /* CategoryItem management */
    'categoryitem created' => 'Element was created',
    'categoryitem not found' => 'Element not found',
    'categoryitem updated' => 'Element was updated',
    'categoryitem deleted' => 'Element was deleted',
];
