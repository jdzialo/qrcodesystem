<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/atraction'], function (Router $router) {
    $router->bind('atraction', function ($id) {
        return app('Modules\Atraction\Repositories\AtractionRepository')->find($id);
    });
    $router->get('atractions', [
        'as' => 'admin.atraction.atraction.index',
        'uses' => 'AtractionController@index',
        'middleware' => 'can:atraction.atractions.index'
    ]);
    $router->get('atractions/create', [
        'as' => 'admin.atraction.atraction.create',
        'uses' => 'AtractionController@create',
        'middleware' => 'can:atraction.atractions.create'
    ]);
    $router->post('atractions', [
        'as' => 'admin.atraction.atraction.store',
        'uses' => 'AtractionController@store',
        'middleware' => 'can:atraction.atractions.create'
    ]);
    $router->get('atractions/{atraction}/edit', [
        'as' => 'admin.atraction.atraction.edit',
        'uses' => 'AtractionController@edit',
        'middleware' => 'can:atraction.atractions.edit'
    ]);
    $router->put('atractions/{atraction}', [
        'as' => 'admin.atraction.atraction.update',
        'uses' => 'AtractionController@update',
        'middleware' => 'can:atraction.atractions.edit'
    ]);
    $router->delete('atractions/{atraction}', [
        'as' => 'admin.atraction.atraction.destroy',
        'uses' => 'AtractionController@destroy',
        'middleware' => 'can:atraction.atractions.destroy'
    ]);
// append
    $router->get('atractions/refresh-places/{atraction}', [
        'as' => 'admin.atraction.atraction.places.refresh',
        'uses' => 'AtractionController@refreshEntityPlaces',
        'middleware' => 'can:atraction.atractions.edit'
    ]);
    $router->get('atractions/refresh-all-places', [
        'as' => 'admin.atraction.atraction.places.all.refresh',
        'uses' => 'AtractionController@refreshAllPlaces',
        'middleware' => 'can:atraction.atractions.edit'
    ]);
    
});
