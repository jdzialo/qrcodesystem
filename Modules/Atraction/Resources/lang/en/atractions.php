<?php

return [
    'list resource' => 'List atractions',
    'create resource' => 'Create atractions',
    'edit resource' => 'Edit atractions',
    'destroy resource' => 'Destroy atractions',
    'title' => [
        'atractions' => 'Atraction',
        'create atraction' => 'Create a atraction',
        'edit atraction' => 'Edit a atraction',
    ],
    'button' => [
        'create atraction' => 'Create a atraction',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
