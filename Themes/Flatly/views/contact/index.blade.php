@extends('layouts.master')

@section('content')
<div class="formarea">
    <div class="row">
        <div class="col-sm-6 col-xs-12 slide">

        </div>
        <div class="col-sm-6 col-xs-12 bluecontainerslim">
            <div class="editarea">
                Strona kontaktowa
            </div>
        </div>
    </div>
    <div class="container">
        <div  class = "col-xs-12 col-sm-6 contactdiv">
            <h1>  Skontaktuj się z nami </h1>
            <br />
            <h5> Siedziba firmy: </h5>
            <h4 class="blue">
                „PRO-FUND” Sp. z o.o. Sp. Kom. 
            </h4>
            <p>
                ul. Szypowskiego 1,
            </p>
            <p>
                39 – 460 Nowa Dęba
            </p>
            <br /><br />
            <p style="font-weight: bolder">  
                Małgorzata Kopala 
            </p>
            <p> 
                tel. 793839669 
            </p>
            <p> 
                e-mail:
                <a href="mailto:m.kopala@profund.com.pl">m.kopala(at)profund.com.pl 
                </a>
            </p>
        </div>
        <div class = "col-xs-12 col-sm-6" >
            {!! Form::open(['route' => 'contactStore', 'method' => 'post', 'class' => 'contactform']) !!}

            @include('contact.partials.create-fields', ['lang' => locale(), 'errors' => $errors])
            {!! Form::close() !!}
        </div>
    </div>



</div>
@stop


@push('scripts')
{!! Theme::script('js/focusblurform.js') !!}
@endpush
