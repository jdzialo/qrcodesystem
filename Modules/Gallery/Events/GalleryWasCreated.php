<?php

namespace Modules\Gallery\Events;

use Modules\Media\Contracts\StoringMedia;
use Modules\Page\Entities\Page;

class GalleryWasCreated implements StoringMedia {

    /**
     * @var array
     */
    public $data;

    /**
     * @var int
     */
    public $galleryId;

    /**
     * @var gallery
     */
    public $gallery;

    public function __construct($galleryId, array $data, $gallery) {
        $this->data = $data;
        $this->galleryId = $galleryId;
        $this->gallery = $gallery;
    }

    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData() {
        return $this->data;
    }

    /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity() {
        return $this->gallery;
    }

}
