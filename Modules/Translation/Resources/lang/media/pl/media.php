<?php

return [
    'gallery' => 'Galeria',
    'title' => [
        'media' => 'Media',
        'edit media' => 'Edytuj media',
    ],
    'breadcrumb' => [
        'media' => 'Media',
    ],
    'table' => [
        'filename' => 'Nazwa pliku',
        'width' => 'Szerokość',
        'height' => 'Wysokość',
    ],
    'form' => [
        'alt_attribute' => 'Atrybut Alt ',
        'description' => 'Opis',
        'keywords' => 'Słowa kluczowe',
    ],
    'validation' => [
        'max_size' => 'Maksymalna wielkość folderu mediów (:size) została osiągnięta.',
    ],
    'file-sizes' => [
        'B' => 'Bytes',
        'KB' => 'Kb',
        'MB' => 'Mb',
        'GB' => 'Gb',
        'TB' => 'Tb',
    ],
    'choose file' => 'Wybierz plik',
    'insert' => 'Wstaw plik',
    'file picker' => 'File Picker',
    'Browse' => 'Przeglądaj ...',
    'gallery' => 'Galeria',
    'thumbnail' => 'Miniaturka',
    'list resource' => 'Lista mediów',
    'create resource' => 'Utwórz media',
    'edit resource' => 'Edytuj media',
    'destroy resource' => 'Usuń media',
];
