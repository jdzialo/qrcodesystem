<?php

return [
   
    'product' => [
        'products' => "Produkty",
        'name' => "Nazwa",
        'description' => "Opis",
        "categoryname" => "Kategoria",
        "undercategoryname" => "Podkategoria",
        "new" => " Nowy " ,
       "category" => "Kategorie" ,
        "editproduct" => "Edycja",
        "addproduct" => "Dodaj",
        "return" => "Powrót",
        "download" => " Pobierz "
        
    ],
    'routing' => [
        'gallery' => 'galeria',
        'contact' => 'kontakt',
         'application-for-disable' => 'zastosowanie/dla-niepelnosprawnych',
        'application-for-industry' => 'zastosowanie/w-przemyśle',
        'application-for-architecture' => 'zastosowanie/w-architekturze'
        
    ],
    'title' => [
        'pages' => 'Strony',
        'create page' => 'Utwórz stronę',
        'edit page' => 'Edytuj stronę',
        'contactpage' => 'Wiadomości kontaktowe'
    ],
    'button' => [
        'create page' => 'Utwórz stronę',
    ],
    'table' => [
        'name' => 'Nazwa',
        'slug' => 'Slug',
    ],
    'form' => [
        'title' => 'Tytuł',
        'slug' => 'Slug',
        'meta_title' => 'Meta tytuł',
        'meta_description' => 'Meta opis',
        'og_title' => 'Facebook tytuł',
        'og_description' => 'Facebook opis',
        'og_type' => 'Facebook typ',
        'template' => 'Szablon strony',
        'is homepage' => 'Strona domowa ?',
        'body' => 'Treść',
    ],
    'validation' => [
        'attributes' => [
            'title' => 'title',
            'body' => 'body',
        ],
    ],
    'facebook-types' => [
        'website' => 'Strona',
        'product' => 'Produkt',
        'article' => 'Artykuł',
    ],
    'navigation' => [
        'back to index' => 'Wróć do indeksu stron',
    ],
    'home' => [
        'title1' => 'Innowacyjne rozwiązania',
        'title2' => 'Przełamuj bariery architektoniczne',
        'section1' => 'Oferujemy  Państwu urządzenie do pokonywania przeszkód poprzecznych, które osoba niepełnosprawna poruszająca się na wózku inwalidzkim jest w stanie obsłużyć samodzielnie. ',
        'section2' => 'Rampę nastawną osoba niepełnosprawna może obsługiwać bez specjalistycznego przeszkolenia, 
                    w prosty sposób - dwoma przyciskami, podnosząc i chowając urządzenia.
                    Z rampą nastawną można w prosty sposób pokonać każdą barierę architektoniczną. 
                    Żaden próg, schodek, czy wysoki krawężnik nie będą już problemem dla wózka. Produkt jest 
                    idealnie dopasowany do potrzeb osób z niepełnosprawnością i nie zagraża w żaden sposób 
                    bezpieczeństwu osoby pokonującej przeszkodę.',
        'section3' => 'Rampa nastawna to produkt umożliwiający osobom poruszającym się na wózku pełny 
                    swobodny udział w życiu, ułatwiając poruszanie się wszędzie tam, gdzie 
                    bariery architektoniczne.',
        'section4' => 'Nadaje się do zamontowania zarówno w miejscu zamieszkania osoby 
                    poruszającej się na wózku, jak i we wszystkich instytucjach dbających 
                    o wyrównywanie szans.
                    Zamów już teraz rampę nastawną, która przyczyni się do polepszenia życia 
                    osób poruszających się na wózku.  Spraw, by znikły bariery architektoniczne, 
                    w twoim otoczeniu.',
        'bottomT1' => 'Zastosowanie dla niepelnosprawnych',
        'bottomT2' => 'Zastosowanie dla przemyslu',
        'bottomT3' => 'Zastosowanie w mieszkaniach',
        'bottomT4' => 'Zastosowanie w architekturze'
        
        
    ]
];
