<?php

return [
    /* Category management */
    'category created' => 'Category zostało utworzone',
    'category not found' => 'Category nie został znaleziony',
    'category updated' => 'Category zostało zaktualizowane',
    'category deleted' => 'Category zostało usunięte.',
    /* CategoryItem management */
    'categoryitem created' => 'Element category utworzony',
    'categoryitem not found' => 'Element category nie został znaleziony',
    'categoryitem updated' => 'Element category zaktualizowany.',
    'categoryitem deleted' => 'Element category usunięty.',
    'category created' => 'Utworzono kategorie',
    'category created' => 'Utworzono podkategorie'
];
