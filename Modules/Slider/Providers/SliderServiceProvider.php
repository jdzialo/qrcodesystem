<?php

namespace Modules\Slider\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Slider\Blade\SliderMultipleDirective;
use Modules\Media\Image\ThumbnailManager;

class SliderServiceProvider extends ServiceProvider {

    use CanPublishConfiguration;

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->registerBindings();

        $this->app->bind('slider.multiple.directive', function () {
            return new SliderMultipleDirective();
        });
    }

    public function boot() {
        $this->publishConfig('slider', 'permissions');
        $this->registerBladeTags();
        $this->registerThumbnails();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array();
    }

    private function registerBindings() {

        $this->app->bind(FinderService::class, function () {
            return new FinderService();
        });

        $this->app->bind(
                'Modules\Slider\Repositories\SliderRepository', function () {
            $repository = new \Modules\Slider\Repositories\Eloquent\EloquentSliderRepository(new \Modules\Slider\Entities\Slider());

            if (!config('app.cache')) {
                return $repository;
            }

            return new \Modules\Slider\Repositories\Cache\CacheSliderDecorator($repository);
        }
        );
        $this->app->bind(
            'Modules\Slider\Repositories\SlideRepository',
            function () {
                $repository = new \Modules\Slider\Repositories\Eloquent\EloquentSlideRepository(new \Modules\Slider\Entities\Slide());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Slider\Repositories\Cache\CacheSlideDecorator($repository);
            }
        );
// add bindings

    }

    private function registerBladeTags() {
//        if (app()->environment() === 'testing') {
//            return;
//        }
        $this->app['blade.compiler']->directive('sliderMultiple', function ($value) {
            return "<?php echo SliderMultipleDirective::show([$value]); ?>";
        });
    }
    
    private function registerThumbnails() {
        $this->app[ThumbnailManager::class]->registerThumbnail('partnerThumb', [
            'fit' => [
                'width' => '215',
                'height' => '150',
                'callback' => function ($constraint) {
                    $constraint->upsize();
                },
            ],
        ]);
    }

}
