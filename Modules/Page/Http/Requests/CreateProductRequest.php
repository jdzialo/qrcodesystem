<?php

namespace Modules\Page\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateProductRequest extends BaseFormRequest
{  

    public function rules()
    {
        return [
            'name' => 'required',
            "file" => "required|mimes:pdf",
            'description' => 'required'
        ];
    }

    public function authorize()
    {
       return true;
    }

    public function messages()
    {
        return [
            'name.required' => "Proszę zamieścić nazwę",
            'file.required' => "Proszę zamieścić plik",
            'description.required' => "Proszę zamieścić opis"
        ];
    }

  
}

