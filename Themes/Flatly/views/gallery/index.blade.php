@extends('layouts.master')
@section('content')

<div class="row">
    <div class="col-sm-6 col-xs-12 slide">

    </div>
    <div class="col-sm-6 col-xs-12 bluecontainerslim">
        <div class="editarea">
            Galeria zdjęć
        </div>
    </div>
</div>

<div class="gallery-image text-center">
    <div class="container">
        <div class="row">
            <?php if (isset($galleries)): ?>
                <?php foreach ($galleries as $image) : ?>    
          
                    <div class="col-sm-3 content-image">
                        <a href="<?php echo $image->path; ?>" data-lightbox="roadtrip" class="image-link">
                            <img src="<?php echo $image->path; ?>" alt="" class="img-responsive content-image-hover"/>
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
          
        </div>
    </div>
</div>

@push('scripts')
{!! Theme::script('vendor/lightbox2/dist/js/lightbox.min.js') !!}
<script>
    lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true ,
       
    })
</script>
@endpush

@if(Session::has('message'))
<div class="alert alert-info">
    {{Session::get('message')}}
</div>
@endif

@stop

