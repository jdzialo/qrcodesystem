<div class="box-body">
    <div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[title]", trans('page::pages.form.title')) !!}
        {!! Form::text("{$lang}[title]", old("{$lang}.title"), ['class' => 'form-control', 'data-slug' => 'source', 'placeholder' => trans('page::pages.form.title'), 'required' => 'required']) !!}
        {!! $errors->first("{$lang}.title", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("{$lang}.slug") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[slug]", trans('page::pages.form.slug')) !!}
        {!! Form::text("{$lang}[slug]", old("{$lang}.slug"), ['class' => 'form-control slug', 'data-slug' => 'target', 'placeholder' => trans('page::pages.form.slug'), 'required' => 'required']) !!}
        {!! $errors->first("{$lang}.slug", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='{{ $errors->has("{$lang}.subtitle") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[subtitle]", trans('page::pages.form.subtitle')) !!}
        <textarea class="form-control" name="{{$lang}}[subtitle]" rows="3" cols="80">{{ old("{$lang}.subtitle") }}</textarea>
        {!! $errors->first("{$lang}.subtitle", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='{{ $errors->has("{$lang}.content_short") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[content_short]", trans('page::pages.form.content_short')) !!}
        <textarea class="form-control" name="{{$lang}}[content_short]" rows="5" cols="80">{{ old("{$lang}.content_short") }}</textarea>
        {!! $errors->first("{$lang}.content_short", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='{{ $errors->has("{$lang}.content") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[content]", trans('page::pages.form.content')) !!}
        <textarea class="ckeditor" name="{{$lang}}[content]" rows="10" cols="80">{{ old("{$lang}.content") }}</textarea>
        {!! $errors->first("{$lang}.content", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("{$lang}.film") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[film]", trans('page::pages.form.film')) !!}
        {!! Form::text("{$lang}[film]", old("{$lang}.film"), ['class' => 'form-control', 'placeholder' => trans('page::pages.form.film')]) !!}
        {!! $errors->first("{$lang}.film", '<span class="help-block">:message</span>') !!}
    </div>
    
    <div class='{{ $errors->has("{$lang}.content2") ? ' has-error' : '' }}'>
        {!! Form::label("{$lang}[content2]", trans('page::pages.form.content')) !!}
        <textarea class="ckeditor" name="{{$lang}}[content2]" rows="10" cols="80">{{ old("{$lang}.content2") }}</textarea>
        {!! $errors->first("{$lang}.content2", '<span class="help-block">:message</span>') !!}
    </div>
    
    <div class="box-group" id="accordion">
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <div class="panel box box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo-{{$lang}}">
                        {{ trans('page::pages.form.meta_data') }}
                    </a>
                </h4>
            </div>
            <div style="height: 0px;" id="collapseTwo-{{$lang}}" class="panel-collapse collapse">
                <div class="box-body">
                    <div class='form-group{{ $errors->has("{$lang}[meta_title]") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}[meta_title]", trans('page::pages.form.meta_title')) !!}
                        {!! Form::text("{$lang}[meta_title]", old("$lang.meta_title"), ['class' => "form-control", 'placeholder' => trans('page::pages.form.meta_title')]) !!}
                        {!! $errors->first("{$lang}[meta_title]", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class='form-group{{ $errors->has("{$lang}[meta_description]") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}[meta_description]", trans('page::pages.form.meta_description')) !!}
                        <textarea class="form-control" name="{{$lang}}[meta_description]" rows="10" cols="80">{{ old("$lang.meta_description") }}</textarea>
                        {!! $errors->first("{$lang}[meta_description]", '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel box box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFacebook-{{$lang}}">
                        {{ trans('page::pages.form.facebook_data') }}
                    </a>
                </h4>
            </div>
            <div style="height: 0px;" id="collapseFacebook-{{$lang}}" class="panel-collapse collapse">
                <div class="box-body">
                    <div class='form-group{{ $errors->has("{$lang}[og_title]") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}[og_title]", trans('page::pages.form.og_title')) !!}
                        {!! Form::text("{$lang}[og_title]", old("{$lang}.og_title"), ['class' => "form-control", 'placeholder' => trans('page::pages.form.og_title')]) !!}
                        {!! $errors->first("{$lang}[og_title]", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class='form-group{{ $errors->has("{$lang}[og_description]") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}[og_description]", trans('page::pages.form.og_description')) !!}
                        <textarea class="form-control" name="{{$lang}}[og_description]" rows="10" cols="80">{{ old("$lang.og_description") }}</textarea>
                        {!! $errors->first("{$lang}[og_description]", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group{{ $errors->has("{$lang}[og_type]") ? ' has-error' : '' }}">
                        <label>{{ trans('page::pages.form.og_type') }}</label>
                        <select class="form-control" name="{{ $lang }}[og_type]">
                            <option value="website" {{ old("$lang.og_type") == 'website' ? 'selected' : '' }}>{{ trans('page::pages.facebook-types.website') }}</option>
                            <option value="product" {{ old("$lang.og_type") == 'product' ? 'selected' : '' }}>{{ trans('page::pages.facebook-types.product') }}</option>
                            <option value="article" {{ old("$lang.og_type") == 'article' ? 'selected' : '' }}>{{ trans('page::pages.facebook-types.article') }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
</div>
