<div class="box-body">

    <div class='form-group' style="color:red">
        <h2>{!! urldecode($product->files()->get()->first()->filename) !!} <i class="fa fa-file-pdf-o"></i></h2>
        <button class="btn btn-default btn-flat edit-button" name="button" type="reset">{{ trans('page::contact.form.changefile') }} <i class="fa fa-exchange" aria-hidden="true"></i> </button>
    </div>

    <div class='file-edit form-group{{ $errors->has("file") ? ' has-error' : '' }}'>
        <label for="files" class="btn btn-primary btn-flat">{{ trans('page::contact.form.uploadfile') }} <i class="fa fa-upload" aria-hidden="true"></i> </label>
        <input name="file" id="files" style="visibility:hidden;" type="file" /> <p id="uploadmessage"></p>
        {!! $errors->first("file", '<span class="help-block">:message</span>') !!}

    </div>
    <div class='form-group{{ $errors->has("name") ? ' has-error' : '' }}'>
        {!! Form::text("name", "$product->name", ['class' => 'form-control', 'placeholder' => trans('page::contact.form.name')]) !!}
        {!! $errors->first("name", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("description") ? ' has-error' : '' }}'>
        {!! Form::textarea("description", "$product->description", ['class' => 'form-control',  'placeholder' => trans('page::contact.form.description')]) !!}
        {!! $errors->first("description", '<span class="help-block">:message</span>') !!}
    </div>


    <div class='form-group{{ $errors->has('format') ? ' has-error' : '' }}'>
        {!! Form::label('format', trans('page::contact.form.format')) !!}
        {!! Form::select('format', ['png' => 'png','jpeg' => 'jpeg','gif' => 'gif'],  $product->files()->get()->first()->extension , ['class' => 'form-control , extension-format']) !!}
        {!! $errors->first('format', '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has('undercategory') ? ' has-error' : '' }}'>
        {!! Form::label('undercategory', trans('page::contact.form.category')) !!}
        {{ Form::select('undercategory', $category , $categoryitemid , ['class' => 'form-control'] )}}

        {!! $errors->first('undercategory', '<span class="help-block">:message</span>') !!}
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}  </button>

        
        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.page.product.indexproducts')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
    </div>


</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#files').change(function () {
            $("#uploadmessage").html("{{trans('page::contact.form.fileupdatemessage') }}"+" " +$('#files').val() );
            
        });


        $(".extension-format").prop('disabled', true);


        $(".file-edit").hide();
        $(".edit-button").click(function () {
            console.log($(".extension-format").attr('disabled'));
            if ($(".extension-format").attr('disabled') == "disabled")
            {
                $(".extension-format").prop('disabled', false);
            } else
            {
                $(".extension-format").prop('disabled', true);
            }

            $(".file-edit").slideToggle();
        });
    });

</script>
