

<div class="box-body">
    <div class='form-group{{ $errors->has("firstname") ? ' has-error' : '' }}'>
        {!! Form::text("firstname", old("firstname"), ['class' => 'form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Imię"', 'placeholder' => trans('page::contact.form.firstname')]) !!}
        {!! $errors->first("firstname", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("lastname") ? ' has-error' : '' }}'>
        {!! Form::text("lastname", old("lastname"), ['class' => 'form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Nazwisko"', 'placeholder' => trans('page::contact.form.lastname')]) !!}
        {!! $errors->first("lastname", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("phone") ? ' has-error' : '' }}'>      
        {!! Form::text("phone", old("phone"), ['class' => 'form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Telefon"',  'placeholder' => trans('page::contact.form.phone')]) !!}
        {!! $errors->first("phone", '<span class="help-block">:message</span>') !!}
    </div>

    <div class='form-group{{ $errors->has("email") ? ' has-error' : '' }}'>
        {!! Form::email("email", old("email"), ['class' => 'form-control circle-input', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="E-mail"',  'placeholder' => trans('page::contact.form.email')]) !!}
        {!! $errors->first("email", '<span class="help-block">:message</span>') !!}
    </div>
    <div class='form-group{{ $errors->has("message") ? ' has-error' : '' }}'>
        {!! Form::textarea("message", old("message"), ['class' => 'form-control circle-input-textarea', 'onfocus' => 'this.placeholder=""', 'onblur' => 'this.placeholder="Wiadomość"',  'placeholder' => trans('page::contact.form.message')]) !!}
        {!! $errors->first("message", '<span class="help-block">:message</span>') !!}
    </div>
      <div class='form-group{{ $errors->has("message") ? ' has-error' : '' }}'>
       {!! app('captcha')->display(); !!}
      </div>
    <div class="form-group">
        {!! Form::submit(trans('page::contact.button.send'), 
        array('class'=>'btn btn-default btn-blue btn50')) !!}
    </div>
    <div>
        <div class="claus">{{ trans('page::contact.contact.agree') }}</div>
    </div>
</div>