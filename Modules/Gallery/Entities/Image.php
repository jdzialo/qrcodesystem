<?php

namespace Modules\Gallery\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use Translatable;

    protected $table = 'gallery__images';
    public $translatedAttributes = ['title', 'content', 'url'];
    protected $fillable = [
        'gallery_id',
        'file_id'
    ];
     public function file() {
        return $this->belongsTo('Modules\Media\Entities\File');
    }

    
}
