<?php

return [
   
    
    'form' => [
        'firstname' => 'First Name',
        'lastname' => 'Last Name',
        'name' => 'Name',
        'email' => 'E-mail',
        'phone' => 'Phone',
        'message' => 'Message',
        'description'=>"Description",
        'instalation_location' => 'description of instalation',
        'category' => "Category" ,
         "format" => "Extension",
        "file" => "File" ,
        'changefile' => "Change file",
        "uploadfile" => "Upload file",
        "fileupdatemessage" => "file uploaded"
    ],
    'button' =>
    [
        'send'=>'Send'
    ],
     'contact' =>
    [
        'data'=>'Contact data',
        'send contactform' => 'You send contact form',
        'receive message' => 'You received a message from',
        'receive offer message' => 'Otrzymałeś wiadomość od' ,
        'agree' => 'Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb niezbędnych do realizacji procesu rekrutacji (zgodnie z Ustawą z dnia 29.08.1997 roku o Ochronie Danych Osobowych; tekst jednolity: Dz. U. z 2002r. Nr 101, poz. 926 ze zm.)'  
        
    
    ],
    'mail' => [
        'subject' => "Formularz kontaktowy"
    ],
    'firstname is required' => 'First Name is required',
    'lastname is required' => 'Last Name is required',
    'email is required' => 'Email is required',
    'phone is required' => 'Phone is required',
      'email is not valid' => 'Email is not valid',
      'phone is not valid' => 'Phone is not valid',
    'message is required' => 'Message is required',
    'captcha is required' => 'Captcha is required',
];
